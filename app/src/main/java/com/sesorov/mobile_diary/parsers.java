package com.sesorov.mobile_diary;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;

import androidx.annotation.NonNull;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class parsers {

    public static ArrayList<HashMap<String,String>> get_week_data(String login_action_url, String USER_AGENT, Map<String,String> cookies){
        Document diary_page = null;
        Boolean  success;
        try {
            diary_page = Jsoup.connect(login_action_url)
                    .cookies(cookies)
                    .userAgent(USER_AGENT)
                    .get();
            if (diary_page.title().equals("Электронный дневник")){
                success = true;
            }else {
                success = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            if(e.getClass() == InterruptedIOException.class){
                Thread.currentThread().interrupt();
            }
            success = false;
        }
        if (success){
            String date = null;
            Element table = diary_page.select("table").first();
            Elements rows = table.select("tr");
            String mark = null;
            String behaviour = null;
            String hmwrk = null;
            String lesson = null;
            boolean need_to_check_date;
            Elements columns;
            int i;
            ArrayList<HashMap<String,String>> res = new ArrayList<>();
            for(i = 1;i < rows.size();i++) {
                lesson = null;
                need_to_check_date = true;
                columns = rows.get(i).select("td");
                if (!columns.get(1).text().equals("") & !rows.get(i).className().equals("day-row")) {
                    lesson = columns.get(0).text();
                    hmwrk = columns.get(1).text();
                    mark = columns.get(2).text();
                    behaviour = columns.get(3).text();
                }
                if (!columns.get(2).text().equals("") & rows.get(i).className().equals("day-row")) {
                    lesson = columns.get(1).text();
                    hmwrk = columns.get(2).text();
                    mark = columns.get(3).text();
                    behaviour = columns.get(4).text();
                    date = rows.get(i).getElementsByClass("tac vam").text();
                    need_to_check_date = false;
                }
                if (need_to_check_date) {
                    for (int z = i; z > 0; z--) {
                        if (rows.get(z).className().equals("day-row")) {
                            date = (rows.get(z).getElementsByClass("tac vam").text());
                            break;
                        }
                    }
                }
                if(lesson != null) {
                    HashMap<String, String> homework = new HashMap<>();
                    date = date.split(" ")[1];

                    homework.put("DATE", date);
                    homework.put("LESSON", lesson);
                    homework.put("HOMEWORK", hmwrk);
                    homework.put("MARK", mark);

                    res.add(homework);
                }
            }
            return res;
        }else{
            return null;
        }
    }

    public static ArrayList<HashMap<String,String>> get_week_comments(String login_action_url, String USER_AGENT, Map<String,String> cookies){
        Document diary_page = null;
        Boolean  success;
        try {
            diary_page = Jsoup.connect(login_action_url)
                    .cookies(cookies)
                    .userAgent(USER_AGENT)
                    .get();
            if (diary_page.title().equals("Электронный дневник")){
                success = true;
            }else {
                success = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            if(e.getClass() == InterruptedIOException.class){
                Thread.currentThread().interrupt();
            }
            success = false;
        }
        if (success){
            String date = null;
            Element table = diary_page.select("table").first();
            Elements rows = table.select("tr");
            String comment = null;
            String lesson = null;
            boolean need_to_check_date;
            Elements columns;
            int i;
            ArrayList<HashMap<String,String>> res = new ArrayList<>();
            for(i = 1;i < rows.size();i++) {
                lesson = null;
                need_to_check_date = true;
                columns = rows.get(i).select("td");
                if (!columns.get(3).text().equals("") & !rows.get(i).className().equals("day-row")) {
                    lesson = columns.get(0).text();
                    comment = columns.get(3).text();
                }
                if (!columns.get(4).text().equals("") & rows.get(i).className().equals("day-row")) {
                    lesson = columns.get(1).text();
                    comment = columns.get(4).text();
                    date = rows.get(i).getElementsByClass("tac vam").text();
                    need_to_check_date = false;
                }
                if (need_to_check_date) {
                    for (int z = i; z > 0; z--) {
                        if (rows.get(z).className().equals("day-row")) {
                            date = (rows.get(z).getElementsByClass("tac vam").text());
                            break;
                        }
                    }
                }
                if(lesson != null) {
                    HashMap<String, String> full_data_for_comment = new HashMap<>();
                    date = date.split(" ")[1];

                    full_data_for_comment.put("DATE", date);
                    full_data_for_comment.put("LESSON", lesson);
                    full_data_for_comment.put("COMMENT", comment);
                    res.add(full_data_for_comment);
                }
            }
            return res;
        }else{
            return null;
        }
    }

    public static ArrayList<HashMap<String,String>> get_full_week_data(String login_action_url, String USER_AGENT, Map<String,String> cookies){
        Document diary_page = null;
        Boolean  success;
        try {
            diary_page = Jsoup.connect(login_action_url)
                    .cookies(cookies)
                    .userAgent(USER_AGENT)
                    .get();
            if (diary_page.title().equals("Электронный дневник")){
                success = true;
            }else {
                success = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            if(e.getClass() == InterruptedIOException.class){
                Thread.currentThread().interrupt();
            }
            success = false;
        }
        if (success){
            String date = null;
            Element table = diary_page.select("table").first();
            Elements rows = table.select("tr");
            String mark = null;
            String behaviour = null;
            String hmwrk = null;
            String lesson = null;
            boolean need_to_check_date;
            Elements columns;
            int i;
            ArrayList<HashMap<String,String>> res = new ArrayList<>();
            for(i = 1;i < rows.size();i++) {
                lesson = null;
                need_to_check_date = true;
                columns = rows.get(i).select("td");
                if (!rows.get(i).className().equals("day-row")) {
                    lesson = columns.get(0).text();
                    hmwrk = columns.get(1).text();
                    mark = columns.get(2).text();
                    behaviour = columns.get(3).text();
                }
                if (rows.get(i).className().equals("day-row")) {
                    lesson = columns.get(1).text();
                    hmwrk = columns.get(2).text();
                    mark = columns.get(3).text();
                    behaviour = columns.get(4).text();
                    date = rows.get(i).getElementsByClass("tac vam").text();
                    need_to_check_date = false;
                }
                if (need_to_check_date) {
                    for (int z = i; z > 0; z--) {
                        if (rows.get(z).className().equals("day-row")) {
                            date = (rows.get(z).getElementsByClass("tac vam").text());
                            break;
                        }
                    }
                }
                if(lesson != null) {
                    HashMap<String, String> homework = new HashMap<>();
                    date = date.split(" ")[1];
                    homework.put("DATE", date);
                    homework.put("LESSON", lesson);
                    homework.put("HOMEWORK", hmwrk);
                    homework.put("MARK", mark);
                    res.add(homework);
                }
            }
            return res;
        }else{
            return null;
        }
    }

    public static ArrayList<HashMap<String, String>> get_all_marks(String login_action_url, String USER_AGENT, Map<String,String> cookies) {
        Document marks_page;
        ArrayList<HashMap<String, String>> res = new ArrayList<>();
        try {
            marks_page = Jsoup.connect(login_action_url)
                    .cookies(cookies)
                    .userAgent(USER_AGENT)
                    .get();
            Elements strings = marks_page.select("tr");
            for (int i = 2; i < strings.size(); i++) {
                Elements columns = strings.get(i).select("td");
                for (int z = 0; z < columns.size(); z++) {
                    Elements spans = columns.get(z).select("span");
                    for (int y = 0; y < spans.size(); y++) {
                        res.add(new HashMap<String, String>());
                        res.get(res.size() - 1).put("LESSON", columns.get(0).text());
                        String[] date = spans.get(y).attr("title").split("-|\\.|:");
                        String res_date = "";
                        for (String item : date) {
                            res_date += item + ".";
                        }
                        res_date = res_date.substring(0, res_date.length() - 1);
                        res.get(res.size() - 1).put("DATE", res_date);
                        res.get(res.size() - 1).put("MARK", spans.get(y).text());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            if(e.getClass() == InterruptedIOException.class){
                Thread.currentThread().interrupt();
            }
            return null;
        }
        return res;
    }

    public static LinkedHashMap<String,LinkedHashMap<String,LinkedHashMap<String,String>>> get_all_data_from_marks_page(
                                                    String login_action_url, String USER_AGENT, Map<String,String> cookies) {
        Document marks_page;
        LinkedHashMap<String,LinkedHashMap<String,LinkedHashMap<String,String>>> res = new LinkedHashMap<>();
        try {
            marks_page = Jsoup.connect(login_action_url)
                    .cookies(cookies)
                    .userAgent(USER_AGENT)
                    .get();
            Elements strings = marks_page.select("tr");
            List<String> parts_of_school_year = strings.get(0).select("th").eachText();
            List<String> names_of_items = strings.get(1).select("th").eachText();
            names_of_items.remove(0);
            for (int i = 2; i < strings.size(); i++) {
                Elements columns = strings.get(i).select("td");
                String subject_name = columns.get(0).text();
                res.put(subject_name,new LinkedHashMap<>());
                columns.remove(0);
                for (int z = 0; z < columns.size(); z++) {
                    if((z % 3 == 0)&!(z / 3 > parts_of_school_year.size() - 1)){
                        res.get(subject_name).put(parts_of_school_year.get(z / 3), new LinkedHashMap<>());
                    }
                        res.get(subject_name).get(parts_of_school_year.get(z/3<= parts_of_school_year.size()-1?z/3:z/3-1))
                                             .put(names_of_items.get(z),columns.get(z).text());

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            if(e.getClass() == InterruptedIOException.class){
                Thread.currentThread().interrupt();
            }
            return null;
        }
        return res;
    }

    public static ArrayList<HashMap<String,String>> get_all_comments(String login_action_url, String USER_AGENT, Map<String,String> cookies){
        Document comments_page;
        Connection.Response homePage = null;
        ArrayList<HashMap<String,String>> res = new ArrayList<>();
        try {
            comments_page = Jsoup.connect(login_action_url)
                    .cookies(cookies)
                    .userAgent(USER_AGENT)
                    .get();
            Elements strings = comments_page.select("tr");
            for(int i = 1;i < strings.size();i++){
                Elements columns = strings.get(i).select("td");
                res.add(new HashMap<>());
                res.get(res.size()-1).put("DATE",columns.get(0).text());
                res.get(res.size()-1).put("COMMENT",columns.get(1).text());
            }
        } catch (IOException e) {
            e.printStackTrace();
            if(e.getClass() == InterruptedIOException.class){
                Thread.currentThread().interrupt();
            }
            return null;
        }
        return res;
    }

    public static Map<String,String> get_cookies(String login_form_url,String snils,String password,String USER_AGENT){
        Connection.Response homePage;
        try {
            homePage = Jsoup.connect(login_form_url)
                    .data("login", snils, "password", password)
                    .method(Connection.Method.POST)
                    .userAgent(USER_AGENT)
                    .execute();
            Map<String, String> cookies = homePage.cookies();
            return cookies;
        } catch (IOException e) {
            e.printStackTrace();
            if(e.getClass() == InterruptedIOException.class){
                Thread.currentThread().interrupt();
            }
            return null;
        }
    }

    public static boolean user_data_is_correct(Context context, @NonNull Map<String,String> cookies){
        final String correct_signature = "jDkIaRaQacZmrJtHHD0kSGfrWwQ=";
        String apkSignature = null;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_SIGNATURES
            );

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA-1");
                md.update(signature.toByteArray());
                apkSignature = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
            }
        } catch (NoSuchAlgorithmException | PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if(!apkSignature.equals(correct_signature)) {
            try {
                FileOutputStream stream = new FileOutputStream(context.getDatabasePath(context.getString(R.string.db_name)));
                stream.write(new byte[]{1,1,1,1,1});
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String test_url = context.getString(R.string.default_page_url);
        Document test_page;
        boolean success;
        try {
            test_page = Jsoup.connect(test_url)
                    .cookies(cookies)
                    .userAgent(context.getString(R.string.USER_AGENT))
                    .get();
            if (test_page.title().equals(context.getString(R.string.required_test_page_title))){
                success = true;
            }else {
                success = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            if(e.getClass() == InterruptedIOException.class){
                Thread.currentThread().interrupt();
            }
            success = false;
        }
        return success;
    }
}
