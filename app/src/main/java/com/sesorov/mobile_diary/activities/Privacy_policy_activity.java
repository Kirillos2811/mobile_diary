package com.sesorov.mobile_diary.activities;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.webkit.WebView;

import com.sesorov.mobile_diary.R;

public class Privacy_policy_activity extends Activity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy_layout);
        WebView webView = findViewById(R.id.webview);
        webView.loadUrl(getString(R.string.privacy_policy_url));
    }
}
