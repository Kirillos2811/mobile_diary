package com.sesorov.mobile_diary.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.sesorov.mobile_diary.BR_Receiver;
import com.sesorov.mobile_diary.DbHelper;
import com.sesorov.mobile_diary.R;
import com.sesorov.mobile_diary.helper_functions;
import com.splunk.mint.Mint;

import net.sqlcipher.database.SQLiteDatabase;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle SavedInstance) {
        SQLiteDatabase.loadLibs(getApplicationContext());
        Mint.initAndStartSession(this.getApplication(), "b1da1e8c");
        setTheme(R.style.SplashScreenTheme);
        super.onCreate(SavedInstance);

        final DbHelper dbhelper = new DbHelper(getApplicationContext(),getString(R.string.db_name),null, getResources().getInteger(R.integer.db_version));
        final SQLiteDatabase db = dbhelper.getReadableDatabase(helper_functions.getSignature(getApplicationContext()));
//        long user_id = dbhelper.insertUser(db, "Шьям", "175-682-213 94", "13coin", 1,
//                "0", "0", "0");
//        ContentValues update_value = new ContentValues();
//        update_value.put(dbhelper.COLUMN_ACTIVE_STATUS, 0);
//        db.update(dbhelper.USER_TABLE_NAME, update_value, "_id != ?", new String[]{Long.toString(user_id)});
        //Checking whether the DB works correctly
//        Cursor user_querry = db.query(DbHelper.USER_TABLE_NAME, null, null,
//                                    null, null, null, null);
//        System.out.println("---------------------------------------------------");
//        while(user_querry.moveToNext()){
//            Log.d("USER_ID", String.valueOf(user_querry.getLong(user_querry.getColumnIndex("_id"))));
//            Log.d("username", user_querry.getString(user_querry.getColumnIndex(DbHelper.COLUMN_USERNAME)));
//            Log.d("snils", user_querry.getString(user_querry.getColumnIndex(DbHelper.COLUMN_SNILS)) + "!!");
//            Log.d("password", user_querry.getString(user_querry.getColumnIndex(DbHelper.COLUMN_PASSWORD)) + "!!");
//        }
//        Cursor homework_querry = db.query(DbHelper.ALL_HOMEWORK_TABLE_NAME, null, DbHelper.COLUMN_DATE + "< ? and user_id = ?",
//                new String[]{String.valueOf(new Date(2019, 8, 12).getTime()), "1"}, null, null, null);
//        System.out.println("---------------------------------------------------");
//        while(homework_querry.moveToNext()){
//            Log.d("DATE", String.valueOf(new Date(homework_querry.getLong(homework_querry.getColumnIndex(DbHelper.COLUMN_DATE)))));
//            Log.d("LESSON", homework_querry.getString(homework_querry.getColumnIndex(DbHelper.COLUMN_LESSON)));
//            Log.d("HOMEWORK", homework_querry.getString(homework_querry.getColumnIndex(DbHelper.COLUMN_HOMEWORK)));
//            Log.d("USER_ID_REFERENCE", String.valueOf(homework_querry.getLong(homework_querry.getColumnIndex(DbHelper.COLUMN_USER_ID_REFERENCE))));
//        }
//        Cursor marks_querry = db.query(DbHelper.ALL_MARKS_TABLE_NAME, null, null,
//                null, null, null, null);
//        System.out.println("---------------------------------------------------");
//        while(marks_querry.moveToNext()){
//            Log.d("DATE", String.valueOf(new Date(marks_querry.getLong(marks_querry.getColumnIndex(DbHelper.COLUMN_DATE)))));
//            Log.d("LESSON", marks_querry.getString(marks_querry.getColumnIndex(DbHelper.COLUMN_LESSON)));
//            Log.d("MARK", marks_querry.getString(marks_querry.getColumnIndex(DbHelper.COLUMN_MARK)));
//            Log.d("USER_ID_REFERENCE", String.valueOf(marks_querry.getLong(marks_querry.getColumnIndex(DbHelper.COLUMN_USER_ID_REFERENCE))));
//        }
//        Cursor comments_querry = db.query(DbHelper.ALL_COMMENTS_TABLE_NAME, null, null,
//                null, null, null, null);
//        System.out.println("---------------------------------------------------");
//        while(comments_querry.moveToNext()){
//            Log.d("DATE", String.valueOf(new Date(comments_querry.getLong(comments_querry.getColumnIndex(DbHelper.COLUMN_DATE)))));
//            Log.d("LESSON", comments_querry.getString(comments_querry.getColumnIndex(DbHelper.COLUMN_LESSON)));
//            Log.d("MARK", comments_querry.getString(comments_querry.getColumnIndex(DbHelper.COLUMN_COMMENT)));
//            Log.d("USER_ID_REFERENCE", String.valueOf(comments_querry.getLong(comments_querry.getColumnIndex(DbHelper.COLUMN_USER_ID_REFERENCE))));
//        }

        final SharedPreferences settings = getSharedPreferences(getString(R.string.app_preferences_name),
                Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = settings.edit();

        long user_to_delete_id = settings.getLong(getString(R.string.user_to_delete ), -1);
        if(user_to_delete_id != -1){
            dbhelper.delete_user_data(db, user_to_delete_id);
        }
        editor.putLong(getString(R.string.user_to_delete), -1);
        editor.apply();

        Intent intent = new Intent(this, BR_Receiver.class);
        sendBroadcast(intent);

        final Cursor user_query = dbhelper.getActiveUser(db);
        if(user_query.getCount()==0){
            Intent authorization_intent = new Intent(this, Authorization_activity.class);
            authorization_intent.putExtra("is_first_authorization", true);
            startActivity(authorization_intent);
        }else{
            String username = user_query.getString(user_query.getColumnIndex(dbhelper.COLUMN_USERNAME));
            String snils = user_query.getString(user_query.getColumnIndex(dbhelper.COLUMN_SNILS));
            String password = user_query.getString(user_query.getColumnIndex(dbhelper.COLUMN_PASSWORD));
            long user_id = user_query.getLong(user_query.getColumnIndex("_id"));

            Intent main_menu_intent = new Intent(this, Main_menu_activity.class);
            main_menu_intent.putExtra("username", username);
            main_menu_intent.putExtra("snils", snils);
            main_menu_intent.putExtra("password", password);
            main_menu_intent.putExtra("user_id", user_id);
            startActivity(main_menu_intent);
        }
        user_query.close();
        dbhelper.close();
        finish();
    }
}