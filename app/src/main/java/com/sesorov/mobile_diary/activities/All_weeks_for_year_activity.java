package com.sesorov.mobile_diary.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.sesorov.mobile_diary.DbHelper;
import com.sesorov.mobile_diary.ExceptionHandling;
import com.sesorov.mobile_diary.JsonUser;
import com.sesorov.mobile_diary.R;
import com.sesorov.mobile_diary.helper_functions;
import com.sesorov.mobile_diary.parsers;

import net.sqlcipher.database.SQLiteDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageButton;
import pl.droidsonroids.gif.GifImageView;

class Week_page_adapter extends FragmentStatePagerAdapter implements ExceptionHandling{
    private List<Date> mondays_of_school_year_weeks;
    private Map<String,String> cookies;
    private Resources res;
    private byte[] binary_cache;
    private int count;
    private String mode;
    private final Gson gson = new Gson();
    Week_page_adapter(@NonNull FragmentManager fm, byte[] binary_cache, String snils, List<Date> mondays_of_school_year_weeks,
                      Context context, Map<String,String> cookies, String mode) {
        super(fm);
        this.mode = mode;
        this.mondays_of_school_year_weeks = mondays_of_school_year_weeks;
        this.res = context.getResources();
        this.cookies = cookies;
        this.binary_cache = binary_cache;
        count = mondays_of_school_year_weeks.size();
    }
    @NonNull
    @Override
    public Fragment getItem(int position) {
        Calendar calendar = helper_functions.get_calendar_without_time_of_day();
        Date first_day_of_week;
        Date last_day_of_week;
        calendar.setTime(this.mondays_of_school_year_weeks.get(position));
        first_day_of_week = calendar.getTime();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        last_day_of_week = calendar.getTime();

        DateFormat user_format = new SimpleDateFormat("dd-MM", Locale.getDefault());
        String title = "с " + user_format.format(first_day_of_week) + " по " + user_format.format(last_day_of_week);

        Bundle arguments = new Bundle();
        arguments.putString("title", title);
        arguments.putStringArray("days_of_week", this.res.getStringArray(R.array.days_of_week));
        arguments.putStringArray("old_values_array_for_replace", this.res.getStringArray(R.array.old_values_array_for_replace));
        arguments.putStringArray("new_values_array_for_replace", this.res.getStringArray(R.array.new_values_array_for_replace));
        arguments.putByteArray("binary_cache", binary_cache);
        arguments.putString("first_day_of_week",first_day_of_week.toString());
        arguments.putString("mode", mode);

        if(cookies!=null) {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String[] splitted_action_url = res.getString(R.string.front_diary_page_url).split("data");
            String login_action_url = splitted_action_url[0] + format.format(first_day_of_week);

            arguments.putString("USER_AGENT", this.res.getString(R.string.USER_AGENT));
            String json_cookies = gson.toJson(cookies);
            arguments.putString("json_cookies", json_cookies);
            arguments.putString("login_action_url", login_action_url);
        }
        Week_fragment week = new Week_fragment();
        week.setArguments(arguments);
        return week;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void onWifiWasTurnedOff() {
        cookies = null;
    }

    int getWeekPosition(Date week){
        return mondays_of_school_year_weeks.indexOf(week);
    }

    Date getCurrentWeek(ViewPager viewpager){
        int position = viewpager.getCurrentItem();
        return mondays_of_school_year_weeks.get(position);
    }

}

public class All_weeks_for_year_activity extends AppCompatActivity implements ExceptionHandling{
    private Map<String, String> cookies;
    private Runnable check_data_parsing;
    private boolean wifiWasTurnedOff = false;
    private Week_page_adapter page_adapter;
    private final Activity activity = this;
    private ViewPager viewpager;
    private Date saved_item;
    private String mode;
    private final Gson gson = new Gson();
    private AlertDialog alertDialog;
    public void initGifImageView(){
        GifImageButton updating_button = findViewById(R.id.updating_button);
        GifDrawable drawable = (GifDrawable) updating_button.getDrawable();
        drawable.stop();
        updating_button.setVisibility(View.VISIBLE);
        updating_button.setOnClickListener(new GifImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = "online";
                wifiWasTurnedOff = false;
                saved_item = page_adapter.getCurrentWeek(viewpager);
                drawable.start();
                Thread thread = new Thread(check_data_parsing);
                thread.start();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SQLiteDatabase.loadLibs(getApplicationContext());
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        final String password = intent.getStringExtra("password");
        final String snils = intent.getStringExtra("snils");
        if(savedInstanceState!=null){
            mode = savedInstanceState.getString("mode");
            saved_item = gson.fromJson(savedInstanceState.getString("saved_item"), Date.class);
        }else{
            mode = intent.getStringExtra("mode");
            saved_item = gson.fromJson(intent.getStringExtra("saved_item"), Date.class);
        }
        Handler UI_handler = new Handler(getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                setContentView(R.layout.viewpager);
                TextView toolbar_header = findViewById(R.id.toolbar_header);
                toolbar_header.setText(R.string.diary_activity_header);

                int current_item_index = msg.arg1;
                page_adapter = (Week_page_adapter)msg.obj;
                viewpager = findViewById(R.id.viewpager);
                viewpager.setAdapter(page_adapter);

                int position = page_adapter.getWeekPosition(saved_item);
                if(position!=-1){
                    viewpager.setCurrentItem(position);
                }else{
                    viewpager.setCurrentItem(current_item_index);
                }
                if(msg.what==0){
                    initGifImageView();
                }
            }
        };

        Handler dialog_handler = new Handler(getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if(page_adapter!=null) {
                    intent.putExtra("saved_item", gson.toJson(page_adapter.getCurrentWeek(viewpager)));
                }else{
                    intent.putExtra("saved_item", gson.toJson(saved_item));
                }
                alertDialog = helper_functions.showAlertDialog(activity, intent, msg.what, wifiWasTurnedOff, mode);
            }
        };

        Runnable main_task = () -> {
            Calendar current_calendar = helper_functions.get_calendar_without_time_of_day();
            Context context = getApplicationContext();
            Resources res = context.getResources();
            Calendar calendar;
            Calendar calendar2;
            DbHelper dbhelper = new DbHelper(context, res.getString(R.string.db_name), null, res.getInteger(R.integer.db_version));
            SQLiteDatabase db = dbhelper.getReadableDatabase(helper_functions.getSignature(context));
            Cursor query = db.query(dbhelper.USER_TABLE_NAME, new String[]{dbhelper.COLUMN_CACHE, "_id"},
                    dbhelper.COLUMN_SNILS + " = ?", new String[]{snils}, null, null, null);
            query.moveToFirst();
            long user_id = query.getLong(query.getColumnIndex("_id"));
            byte[] binary_cache = query.getBlob(query.getColumnIndex(dbhelper.COLUMN_CACHE));
            JsonUser json_user = gson.fromJson(new String(binary_cache), JsonUser.class);
            query.close();
            HashMap<String, ArrayList<Date>> mondays_of_school_year_weeks_cache = json_user.get_mondays_of_school_year_weeks_cache();
            Date date_when_updated = mondays_of_school_year_weeks_cache
                                    .get(getString(R.string.date_when_was_updated)).get(0);

            List<Date> mondays_of_school_year_weeks  = mondays_of_school_year_weeks_cache.get(getString(R.string.mondays));
            if(!current_calendar.getTime().equals(date_when_updated) && cookies != null){
                Calendar[] first_and_last_mondays_of_the_school_year = helper_functions.
                            get_first_and_last_week_mondays(getApplicationContext(), cookies);
                if(first_and_last_mondays_of_the_school_year != null) {
                    mondays_of_school_year_weeks = new ArrayList<>();
                    calendar = first_and_last_mondays_of_the_school_year[0];
                    calendar2 = first_and_last_mondays_of_the_school_year[1];
                    Calendar calendar_copy = (Calendar) calendar.clone();
                    Calendar calendar2_copy = (Calendar) calendar2.clone();
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            helper_functions.set_user_cache(db, context, json_user, cookies, user_id,
                                    res, calendar_copy, calendar2_copy);
                        }
                    });
                    thread.start();
                    while (calendar.before(calendar2) || calendar.equals(calendar2)) {
                        mondays_of_school_year_weeks.add(calendar.getTime());
                        calendar.add(Calendar.WEEK_OF_YEAR, 1);
                    }
                }
            }

            int current_item_index = mondays_of_school_year_weeks.size()-1;
            current_calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            for (int i = 0; i < mondays_of_school_year_weeks.size(); i++) {
                if (current_calendar.getTime().equals(mondays_of_school_year_weeks.get(i))) {
                    current_item_index = i;
                    break;
                }
            }

            int cache_storing_limit = getResources().getInteger(R.integer.cache_storing_limit_in_weeks);
            if (cookies == null) {
                int min_index = current_item_index - cache_storing_limit;
                int max_index = current_item_index + cache_storing_limit + 1;
                current_item_index = cache_storing_limit;
                if(min_index < 0){
                    current_item_index += min_index;
                    min_index = 0;
                }
                if(max_index > mondays_of_school_year_weeks.size()){
                    max_index = mondays_of_school_year_weeks.size();
                }
                mondays_of_school_year_weeks = mondays_of_school_year_weeks.subList(min_index, max_index);
            }
            page_adapter = new Week_page_adapter(getSupportFragmentManager(), binary_cache, snils,
                    mondays_of_school_year_weeks, getApplicationContext(), cookies, mode);
            Message msg = new Message();
            msg.obj = page_adapter;
            msg.arg1 = current_item_index;
            msg.what = cookies != null ? 1 : 0;
            UI_handler.sendMessage(msg);
        };
        check_data_parsing = () -> {
            //Getting cookies
            cookies = parsers.get_cookies(getString(R.string.login_form_url), snils, password,
                    getString(R.string.USER_AGENT));
            Message msg = new Message();

            if (!helper_functions.isOnline(getApplicationContext())) {
                msg.what = R.string.is_not_online_warning;
                dialog_handler.sendMessage(msg);
            } else if (cookies == null) {
                msg.what = R.string.cloud_service_error_warning_for_dialogs;
                dialog_handler.sendMessage(msg);
            }else if(!helper_functions.user_has_a_diary(activity, cookies)){
                msg.what = R.string.whether_to_load_cached_data_instead_of_diary;
                dialog_handler.sendMessage(msg);
            }else{
                wifiWasTurnedOff = false;
                Thread thread = new Thread(main_task);
                thread.start();
            }
        };
        setContentView(R.layout.waiting_layout);
        GifImageView gif_view = findViewById(R.id.waiting_gif);
        gif_view.postOnAnimationDelayed(() -> {
            Thread thread;
            if(mode.equals("online")) {
                thread = new Thread(check_data_parsing);
            }else{
                thread = new Thread(main_task);
            }
            thread.start();
        },200);
    }

    @Override
    public void onWifiWasTurnedOff() {
        if(!wifiWasTurnedOff && page_adapter != null){
            page_adapter.onWifiWasTurnedOff();
            Thread thread = new Thread(check_data_parsing);
            thread.start();
            wifiWasTurnedOff = true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(page_adapter != null && viewpager != null) {
            saved_item = page_adapter.getCurrentWeek(viewpager);
        }
        outState.putString("saved_item", gson.toJson(saved_item));
        outState.putString("mode", mode);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        if(alertDialog != null){
            alertDialog.dismiss();
        }
        super.onDestroy();
    }
}
