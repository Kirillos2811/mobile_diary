package com.sesorov.mobile_diary.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.sesorov.mobile_diary.DbHelper;
import com.sesorov.mobile_diary.R;
import com.sesorov.mobile_diary.helper_functions;

import net.sqlcipher.database.SQLiteDatabase;

public class Settings_activity extends AppCompatActivity {
    private Switch mark_switch, homework_switch, comment_switch;
    private Spinner mark_spinner_for_symbol_values, mark_spinner_for_numeric_values;
    private DbHelper dbhelper;
    private SQLiteDatabase db;
    private String[] numeric_values_array, symbol_values_array;
    private ArrayAdapter<String> numeric_spinner_adapter, symbol_spinner_adapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        SQLiteDatabase.loadLibs(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setTitle(R.string.notifications_settings_title_text);
        TextView autostart_enabling_guide = findViewById(R.id.autostart_enabling_guide);
        String guide = getString(R.string.autostart_enabling_guide_text)
                .replace("APP_NAME", getString(R.string.app_name));
        autostart_enabling_guide.setText(guide);
        mark_switch = findViewById(R.id.mark_notification_setting_switch);
        homework_switch = findViewById(R.id.homework_notification_setting_switch);
        comment_switch = findViewById(R.id.comment_notification_setting_switch);
        mark_spinner_for_symbol_values = findViewById(R.id.marks_notification_setting_spinner_for_symbol_values);
        mark_spinner_for_numeric_values = findViewById(R.id.marks_notification_setting_spinner_for_numeric_values);
        numeric_values_array = getResources().getStringArray(R.array.mark_notification_setting_numeric_values);
        numeric_spinner_adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item, numeric_values_array);
        numeric_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        symbol_values_array = getResources().getStringArray(R.array.mark_notification_setting_symbol_values);
        symbol_spinner_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, symbol_values_array);
        symbol_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mark_spinner_for_symbol_values.setAdapter(symbol_spinner_adapter);
        mark_spinner_for_numeric_values.setAdapter(numeric_spinner_adapter);

        String password = getIntent().getStringExtra("password");

        dbhelper  = new DbHelper(this, getString(R.string.db_name),
                null, getResources().getInteger(R.integer.db_version));
        db = dbhelper.getReadableDatabase(helper_functions.getSignature(getApplicationContext()));
        Cursor query = db.query(dbhelper.USER_TABLE_NAME,null,dbhelper.COLUMN_PASSWORD + " = ?",
                new String[]{password},null,null,null);
        query.moveToFirst();
        String user_mark_notification_setting = query.getString(query.getColumnIndex(dbhelper.COLUMN_MARK_NOTIFICATIONS_SETTING));
        String user_homework_notification_setting = query.getString(query.getColumnIndex(dbhelper.COLUMN_HOMEWORK_NOTIFICATIONS_SETTING));
        String user_comment_notification_setting = query.getString(query.getColumnIndex(dbhelper.COLUMN_COMMENT_NOTIFICATIONS_SETTING));

        TextView autostart_permission_button = findViewById(R.id.autostart_permission_button);
        autostart_permission_button.setOnClickListener(v -> {
            Intent request_intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            startActivity(request_intent);
        });
        //initializing spinner listeners
        Spinner.OnItemSelectedListener listener_for_symbol_spinner = new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update_user_mark_setting(password);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
        Spinner.OnItemSelectedListener listener_for_numeric_spinner = new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    update_user_mark_setting(password);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
        //initializing switch listeners
        mark_switch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                mark_spinner_for_symbol_values.setVisibility(View.VISIBLE);
                mark_spinner_for_numeric_values.setVisibility(View.VISIBLE);

                mark_spinner_for_symbol_values.setOnItemSelectedListener(listener_for_symbol_spinner);
                mark_spinner_for_numeric_values.setOnItemSelectedListener(listener_for_numeric_spinner);
                update_user_mark_setting(password);
            }else{
                mark_spinner_for_symbol_values.setVisibility(View.INVISIBLE);
                mark_spinner_for_numeric_values.setVisibility(View.INVISIBLE);

                mark_spinner_for_symbol_values.setOnItemSelectedListener(null);
                mark_spinner_for_numeric_values.setOnItemSelectedListener(null);

                mark_spinner_for_numeric_values.setSelection(numeric_spinner_adapter.getPosition("5"));
                mark_spinner_for_symbol_values.setSelection(symbol_spinner_adapter.getPosition("<="));
                ContentValues new_user_values = new ContentValues();
                new_user_values.put(dbhelper.COLUMN_MARK_NOTIFICATIONS_SETTING,"0");
                db.update(dbhelper.USER_TABLE_NAME, new_user_values, dbhelper.COLUMN_PASSWORD + " = ?", new String[]{password});
            }
        });
        homework_switch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            ContentValues new_user_values = new ContentValues();
            if(isChecked){
                new_user_values.put(dbhelper.COLUMN_HOMEWORK_NOTIFICATIONS_SETTING,"1");
                db.update(dbhelper.USER_TABLE_NAME, new_user_values, dbhelper.COLUMN_PASSWORD + " = ?", new String[]{password});
            }else{
                new_user_values.put(dbhelper.COLUMN_HOMEWORK_NOTIFICATIONS_SETTING,"0");
                db.update(dbhelper.USER_TABLE_NAME, new_user_values, dbhelper.COLUMN_PASSWORD + " = ?", new String[]{password});
            }
        });
        comment_switch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            ContentValues new_user_values = new ContentValues();
            if(isChecked){
                new_user_values.put(dbhelper.COLUMN_COMMENT_NOTIFICATIONS_SETTING,"1");
                db.update(dbhelper.USER_TABLE_NAME, new_user_values, dbhelper.COLUMN_PASSWORD + " = ?", new String[]{password});
            }else{
                new_user_values.put(dbhelper.COLUMN_COMMENT_NOTIFICATIONS_SETTING,"0");
                db.update(dbhelper.USER_TABLE_NAME, new_user_values, dbhelper.COLUMN_PASSWORD + " = ?", new String[]{password});
            }
        });
        //Updating widgets
        if(user_homework_notification_setting.equals("1")){
            homework_switch.setChecked(true);
        }
        if(user_comment_notification_setting.equals("1")){
            comment_switch.setChecked(true);
        }
        if(!user_mark_notification_setting.equals("0")){
            initialize_spinners(user_mark_notification_setting);
        }else{
            mark_spinner_for_numeric_values.setSelection(numeric_spinner_adapter.getPosition("5"));
            mark_spinner_for_symbol_values.setSelection(symbol_spinner_adapter.getPosition("<="));
        }
    }
    private void initialize_spinners(String user_mark_notification_setting){
        String symbol_spinner_value = user_mark_notification_setting.substring(0,2);
        String numeric_spinner_value = String.valueOf(user_mark_notification_setting.charAt(2));
        mark_spinner_for_symbol_values.setSelection(symbol_spinner_adapter.getPosition(symbol_spinner_value));
        mark_spinner_for_numeric_values.setSelection(numeric_spinner_adapter.getPosition(numeric_spinner_value));
        mark_switch.setChecked(true);
    }
    private void update_user_mark_setting(String password){
        ContentValues new_user_values = new ContentValues();
        String new_symbol_value = (String)mark_spinner_for_symbol_values.getSelectedItem();
        String new_numeric_value = (String)mark_spinner_for_numeric_values.getSelectedItem();
        new_user_values.put(dbhelper.COLUMN_MARK_NOTIFICATIONS_SETTING,new_symbol_value + new_numeric_value);
        db.update(dbhelper.USER_TABLE_NAME, new_user_values, dbhelper.COLUMN_PASSWORD + " = ?", new String[]{password});
    }

}
