package com.sesorov.mobile_diary.activities;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import com.sesorov.mobile_diary.JsonUser;
import com.sesorov.mobile_diary.R;
import com.sesorov.mobile_diary.helper_functions;
import com.sesorov.mobile_diary.parsers;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Week_fragment extends Fragment {
    //This is the main list for  all the data for the week
    private ArrayList<ArrayList<HashMap<String, String>>> list = new ArrayList<>();
    private ArrayList<HashMap<String, String>> data;
    private List<Map<String, String>> days_of_week = new ArrayList<>();
    private final Gson gson = new Gson();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Bundle bundle = getArguments();
        View view = inflater.inflate(R.layout.week_of_year_layout, container,false);

        final String title = bundle.getString("title");

        if(savedInstanceState!=null){
            String json_list = savedInstanceState.getString("json_list");
            list = gson.fromJson(json_list,list.getClass());

            String json_data = savedInstanceState.getString("json_data");
            data = gson.fromJson(json_data, new TypeToken<ArrayList<HashMap<String, String>>>(){}.getType());

            String json_days_of_week = savedInstanceState.getString("json_days_of_week");
            days_of_week = gson.fromJson(json_days_of_week, days_of_week.getClass());
        }else {
            String[] old_values_array_for_replace = bundle.getStringArray("old_values_array_for_replace");
            String[] new_values_array_for_replace = bundle.getStringArray("new_values_array_for_replace");

            String json_cookies = bundle.getString("json_cookies");

            if (json_cookies != null) {
                String USER_AGENT = bundle.getString("USER_AGENT");
                String login_action_url = bundle.getString("login_action_url");

                Map<String, String> cookies = gson.fromJson(json_cookies, new TypeToken<Map<String, String>>() {
                }.getType());
                Callable task = new Callable() {
                    @Override
                    public ArrayList<HashMap<String, String>> call() {
                        ArrayList<HashMap<String, String>> data = parsers.get_full_week_data(login_action_url, USER_AGENT, cookies);
                        return data;
                    }
                };
                FutureTask<ArrayList<HashMap<String, String>>> future = new FutureTask<>(task);
                Thread thread = new Thread(future);
                thread.start();
                try {
                    data = future.get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            if(data==null){
                String first_day_of_week = bundle.getString("first_day_of_week");
                byte[] binary_cache = bundle.getByteArray("binary_cache");
                JsonUser json_user = gson.fromJson(new String(binary_cache), JsonUser.class);
                data = json_user.get_all_weeks_activity_cache().get(first_day_of_week);
            }
            if(data!=null) {
                String last_date = null;
                for (HashMap<String, String> item : data) {
                    if (!item.get("DATE").equals(last_date)) {
                        Calendar calendar = helper_functions.get_calendar_without_time_of_day();
                        calendar.setTime(helper_functions.get_date_from_string(item.get("DATE")));
                        int number_of_day = calendar.get(Calendar.DAY_OF_WEEK);
                        HashMap<String, String> day_of_week = new HashMap<>();
                        day_of_week.put("day_of_week", bundle.getStringArray("days_of_week")[number_of_day-2]);
                        days_of_week.add(day_of_week);
                        list.add(new ArrayList<>());
                    }
                    last_date = item.get("DATE");
                    item.remove("DATE");
                    if (item.get("MARK").equals("Отсутствовал")) {
                        item.remove("MARK");
                        item.put("MARK", "Н");
                    }
                    for (int i = 0; i < old_values_array_for_replace.length; i++) {
                        if (item.get("LESSON").equals(old_values_array_for_replace[i])) {
                            item.remove("LESSON");
                            item.put("LESSON", new_values_array_for_replace[i]);
                        }
                    }
                    list.get(list.size() - 1).add(item);
                }
            }
        }

        TextView textview = view.findViewById(R.id.week_from_to);
        textview.setText(title);

        if(list.size() > 0) {
            SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(
                    getContext(), days_of_week,
                    android.R.layout.simple_expandable_list_item_1, new String[]{"day_of_week"},
                    new int[]{android.R.id.text1}, list, R.layout.lesson_of_week_layout,
                    new String[]{"LESSON", "HOMEWORK", "MARK"}, new int[]{R.id.LESSON, R.id.HOMEWORK, R.id.MARK});
            ExpandableListView expandable_list_view = view.findViewById(R.id.list_of_days_of_week);
            expandable_list_view.setAdapter(adapter);
        }else if(data == null){
            ((All_weeks_for_year_activity) getActivity()).onWifiWasTurnedOff();
            ImageView exception_imageview = view.findViewById(R.id.exception_imageview);
            exception_imageview.setVisibility(View.VISIBLE);
        }else{
            TextView no_data_warning = view.findViewById(R.id.no_data_warning_textview);
            no_data_warning.setVisibility(View.VISIBLE);
        }

        ImageButton left_arrow = view.findViewById(R.id.left_arrow);
        ImageButton right_arrow = view.findViewById(R.id.right_arrow);
        left_arrow.setOnClickListener(v -> {
            ViewPager viewpager = (ViewPager) view.getParent();
            viewpager.setCurrentItem(viewpager.getCurrentItem()-1, true);
        });
        right_arrow.setOnClickListener(v -> {
            ViewPager viewpager = (ViewPager) view.getParent();
            viewpager.setCurrentItem(viewpager.getCurrentItem()+1, true);
        });
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        String json_list = gson.toJson(list);
        outState.putString("json_list", json_list);
        String json_data = gson.toJson(data);
        outState.putString("json_data", json_data);
        String json_days_of_week = gson.toJson(days_of_week);
        outState.putString("json_days_of_week", json_days_of_week);
    }
}
