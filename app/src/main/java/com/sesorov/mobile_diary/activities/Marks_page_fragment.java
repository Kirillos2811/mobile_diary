package com.sesorov.mobile_diary.activities;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.sesorov.mobile_diary.R;

public class Marks_page_fragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Bundle bundle = getArguments();
        final Context context = getContext();
        View view = inflater.inflate(R.layout.marks_layout, container,false);
        final String title = bundle.getString("title");
        final String[] headers = bundle.getStringArray("headers");
        final String[] old_values_array_for_replace = bundle.getStringArray("old_values_array_for_replace");
        final String[] new_values_array_for_replace = bundle.getStringArray("new_values_array_for_replace");

        TextView title_text = view.findViewById(R.id.table_title);
        title_text.setText(title);
        TableLayout table = view.findViewById(R.id.marks_table);
        TableRow items_row = new TableRow(getContext());
        TableRow.LayoutParams row_params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams column_params = new TableRow.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        items_row.setLayoutParams(row_params);
        TextView item_name = new TextView(context);
        item_name.setText("Предмет");
        item_name.setLayoutParams(column_params);
        item_name.setGravity(Gravity.CENTER);
        item_name.setBackgroundResource(R.drawable.cell_shape);
        items_row.addView(item_name);
        int i = 1;
        for(String header:headers){
            item_name = new TextView(context);
            item_name.setBackgroundResource(R.drawable.cell_shape);
            item_name.setText(header);
            item_name.setGravity(Gravity.CENTER);
            if(header.contains("оценки")|header.contains("ОЦЕНКИ")){
                table.setColumnShrinkable(i,true);
            }
            items_row.addView(item_name);
            item_name.setLayoutParams(column_params);
            i++;
        }
        table.addView(items_row);
        String[] lessons = bundle.getStringArray("lessons");
        for(String lesson:lessons){
            TableRow row = new TableRow(context);
            row.setLayoutParams(row_params);
            TextView lesson_name = new TextView(context);
            lesson_name.setText(lesson);
            for (int z = 0; z < old_values_array_for_replace.length; z++) {
                if (lesson.equals(old_values_array_for_replace[z])) {
                    lesson_name.setText(new_values_array_for_replace[z]);
                    break;
                }
            }
            lesson_name.setLayoutParams(column_params);
            lesson_name.setGravity(Gravity.CENTER);
            lesson_name.setBackgroundResource(R.drawable.cell_shape);
            row.addView(lesson_name);
            for(String item_text:bundle.getStringArray(lesson)){
                TextView new_item = new TextView(context);
                new_item.setText(item_text);
                new_item.setLayoutParams(column_params);
                new_item.setGravity(Gravity.CENTER);
                new_item.setBackgroundResource(R.drawable.cell_shape);
                row.addView(new_item);
            }
            table.addView(row);
        }
        ImageButton left_arrow = view.findViewById(R.id.left_arrow);
        ImageButton right_arrow = view.findViewById(R.id.right_arrow);
        left_arrow.setOnClickListener(v -> {
            ViewPager viewpager = (ViewPager) view.getParent();
            viewpager.setCurrentItem(viewpager.getCurrentItem()-1, true);
        });
        right_arrow.setOnClickListener(v -> {
            ViewPager viewpager = (ViewPager) view.getParent();
            viewpager.setCurrentItem(viewpager.getCurrentItem()+1, true);
        });
        return view;
    }
}
