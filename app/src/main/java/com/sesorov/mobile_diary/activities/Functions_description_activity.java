package com.sesorov.mobile_diary.activities;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.sesorov.mobile_diary.R;

public class Functions_description_activity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.functions_description_title_text);
        setContentView(R.layout.functions_description_layout);
    }
}
