package com.sesorov.mobile_diary.activities;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.sesorov.mobile_diary.DbHelper;
import com.sesorov.mobile_diary.R;
import com.sesorov.mobile_diary.helper_functions;
import com.sesorov.mobile_diary.parsers;

import net.sqlcipher.database.SQLiteDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
//TODO:если нажать на крестик, когда шкала прогресса доходит до оценок и кеша, то выводится неправильное сообщение о сбое в получении данных.
class AuthoriozationRunnable implements Runnable {
    private Authorization_activity activity = null;
    @Override
    public void run() {
        DbHelper dbhelper = activity.dbhelper;
        SQLiteDatabase db = activity.db;
        String snils = activity.snils;
        String password = activity.password;
        String username = activity.username;
        String USER_AGENT = activity.USER_AGENT;

        final SharedPreferences settings = activity.getSharedPreferences(activity.getString(R.string.app_preferences_name),
                Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = settings.edit();
        String login_form_url = activity.getString(R.string.login_form_url);
        //Getting authorization cookies
        Map<String, String> cookies = parsers.get_cookies(login_form_url, snils, password, USER_AGENT);
        if (cookies == null) {
            if (Thread.currentThread().isInterrupted()) {
                activity.interruptionHandler.sendEmptyMessage(0);
            } else {
                activity.warning_message_handler.sendEmptyMessage(R.string.cloud_service_error_warning);
            }
        } else if (!parsers.user_data_is_correct(activity.context, cookies)) {
            if (Thread.currentThread().isInterrupted()) {
                activity.interruptionHandler.sendEmptyMessage(0);
            } else {
                activity.warning_message_handler.sendEmptyMessage(R.string.wrong_snils_or_password_warning);
            }
        } else {
            //the active user has isActive = 1
            long user_id = dbhelper.insertUser(db, username, snils, password, 1,
                    "0", "0", "0");

            //setting SharedPreferences
            editor.putLong(activity.getString(R.string.user_to_delete), user_id);
            editor.apply();
            activity.progress_bar_handler.sendEmptyMessage(100);
            try {
                Calendar calendar;
                Calendar calendar2;
                Calendar calendar_copy;
                Calendar calendar2_copy;

                Calendar[] first_and_last_mondays_of_the_school_year = helper_functions.get_first_and_last_week_mondays(activity, cookies);
                calendar = first_and_last_mondays_of_the_school_year[0];
                calendar_copy = (Calendar) calendar.clone();
                calendar2 = first_and_last_mondays_of_the_school_year[1];
                calendar2_copy = (Calendar) calendar2.clone();

                String[] splitted_action_url = activity.getString(R.string.front_diary_page_url).split("data");
                String login_action_url;

                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                while (calendar.before(calendar2) || calendar.equals(calendar2)) {
                    activity.progress_bar_handler.sendEmptyMessage(20);
                    login_action_url = splitted_action_url[0] + format.format(calendar.getTime());
                    //Getting homework for one week
                    ArrayList<HashMap<String, String>> week_homework = parsers.get_week_data(login_action_url, USER_AGENT, cookies);

                    //Pushing homework into DB
                    for (HashMap<String, String> element : week_homework) {
                        dbhelper.insertHomework(db, helper_functions.get_date_from_string(element.get("DATE")).getTime(),
                                helper_functions.getWeekNumber(calendar.getTime()),
                                element.get("LESSON"), element.get("HOMEWORK"), 1, user_id);
                    }
                    //Getting comments for one week
                    ArrayList<HashMap<String, String>> week_comments = parsers.get_week_comments(login_action_url, USER_AGENT, cookies);

                    //Pushing comments into DB
                    for (HashMap<String, String> element : week_comments) {
                        dbhelper.insertComment(db, helper_functions.get_date_from_string(element.get("DATE")).getTime(),
                                element.get("LESSON"),
                                helper_functions.getWeekNumber(calendar.getTime()),
                                element.get("COMMENT"), user_id);
                    }

                    calendar.add(Calendar.WEEK_OF_YEAR, 1);
                }
                //Getting marks from marks_page and comments from comments_page

                ArrayList<HashMap<String, String>> all_marks = parsers.get_all_marks(
                        activity.getString(R.string.marks_for_the_year_url),
                        USER_AGENT,
                        cookies);

                ArrayList<HashMap<String, String>> all_comments = parsers.get_all_comments(
                        activity.getString(R.string.all_comments_url),
                        USER_AGENT,
                        cookies);

                for (HashMap<String, String> item : all_marks) {
                    Date date = helper_functions.get_date_from_string(item.get("DATE"));

                    dbhelper.insertMark(db, date.getTime(),
                            helper_functions.getWeekNumber(date), item.get("LESSON"), item.get("MARK"), user_id);

                }

                for (HashMap<String, String> item : all_comments) {
                    Date date = helper_functions.get_date_from_string(item.get("DATE"));

                    dbhelper.insertComment(db, date.getTime(), null, helper_functions.getWeekNumber(date), item.get("COMMENT"), user_id);

                }
                activity.progress_bar_handler.sendEmptyMessage((activity.progressbar.getMax() - activity.progressbar.getProgress()) / 2);

                helper_functions.set_user_cache(db, activity,null, cookies, user_id,
                                                activity.getResources(), calendar_copy, calendar2_copy);
                activity.progress_bar_handler.sendEmptyMessage(activity.progressbar.getMax() - activity.progressbar.getProgress());

                Intent main_menu_intent = new Intent(activity, Main_menu_activity.class);
                main_menu_intent.putExtra("username", username);
                main_menu_intent.putExtra("snils", snils);
                main_menu_intent.putExtra("password", password);
                main_menu_intent.putExtra("user_id", user_id);

                activity.startActivity(main_menu_intent);
                //others have isActive = 0
                ContentValues update_value = new ContentValues();
                update_value.put(dbhelper.COLUMN_ACTIVE_STATUS, 0);
                db.update(dbhelper.USER_TABLE_NAME, update_value, "_id != ?", new String[]{Long.toString(user_id)});
            } catch (NullPointerException e) {
                e.printStackTrace();
                if (!Thread.currentThread().isInterrupted()) {
                    activity.warning_message_handler.sendEmptyMessage(R.string.parsing_problems_occured_warning);
                }
                dbhelper.delete_user_data(db, user_id);
            } catch(IllegalArgumentException e){
                e.printStackTrace();
                activity.warning_message_handler.sendEmptyMessage(R.string.no_diaries_were_found);
                dbhelper.delete_user_data(db, user_id);
            }


            editor.putLong(activity.getString(R.string.user_to_delete), -1);
            editor.apply();

            if (Thread.currentThread().isInterrupted()) {
                activity.interruptionHandler.sendEmptyMessage(0);
            }
        }
    }

    public void link(Authorization_activity activity){
        this.activity = activity;
    }
}

public class Authorization_activity extends AppCompatActivity {
    DbHelper dbhelper;
    SQLiteDatabase db;
    private boolean is_first_authorization;
    private boolean parsing_was_cancelled = false;
    ProgressBar progressbar;
    String snils, password, username, USER_AGENT;
    Context context;
    private AuthoriozationRunnable runnable;
    private Thread parsing_thread;
    private ArrayList<String> usernames, snilses, passwords;
    private EditText name_edit, login_edit, password_edit;
    private ListView users_list;
    private Button sign_up_button;
    private ImageButton cancel_button;
    private LinearLayout progress_bar_layout;
    //Listeners
    private final ListView.OnItemClickListener users_list_listener  = new ListView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView username_view = (TextView) view;
            Cursor user_query = db.query(dbhelper.USER_TABLE_NAME, null, dbhelper.COLUMN_USERNAME + " = ?",
                    new String[]{username_view.getText().toString()}, null, null, null);
            user_query.moveToFirst();
            long chosen_user_id = user_query.getLong(user_query.getColumnIndex("_id"));
            String chosen_user_snils = user_query.getString(user_query.getColumnIndex(dbhelper.COLUMN_SNILS));
            String chosen_user_password = user_query.getString(user_query.getColumnIndex(dbhelper.COLUMN_PASSWORD));

            ContentValues chosen_user_values = new ContentValues();
            chosen_user_values.put(dbhelper.COLUMN_ACTIVE_STATUS, 1);

            ContentValues other_users_values = new ContentValues();
            other_users_values.put(dbhelper.COLUMN_ACTIVE_STATUS, 0);

            db.update(dbhelper.USER_TABLE_NAME, chosen_user_values, "_id = ?", new String[]{String.valueOf(chosen_user_id)});
            db.update(dbhelper.USER_TABLE_NAME, other_users_values, "_id != ?", new String[]{String.valueOf(chosen_user_id)});

            Intent main_menu_intent = new Intent(context, Main_menu_activity.class);
            main_menu_intent.putExtra("username", username_view.getText().toString());
            main_menu_intent.putExtra("snils", chosen_user_snils);
            main_menu_intent.putExtra("password", chosen_user_password);
            main_menu_intent.putExtra("user_id", chosen_user_id);
            startActivity(main_menu_intent);
            finish();
        }
    };
    private final View.OnClickListener cancel_button_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            parsing_thread.interrupt();
            parsing_was_cancelled = true;
        }
    };
    private final View.OnClickListener sign_up_button_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            username = name_edit.getText().toString();
            snils = login_edit.getText().toString();
            password = password_edit.getText().toString();
            if (username.equals("") | snils.equals("") | password.equals("")) {
                Toast.makeText(context, getString(R.string.fill_all_fields_warning), Toast.LENGTH_SHORT).show();
            } else if(usernames != null && usernames.contains(username)) {
                Toast.makeText(context, getString(R.string.not_unique_username_warning), Toast.LENGTH_SHORT).show();
            } else if(snilses != null && snilses.contains(snils)){
                Toast.makeText(context, getString(R.string.not_unique_snils_warning), Toast.LENGTH_SHORT).show();
            } else if(passwords != null && passwords.contains(password)){
                Toast.makeText(context, getString(R.string.not_unique_password_warning), Toast.LENGTH_SHORT).show();
            } else if (!helper_functions.isOnline(context)) {
                Toast.makeText(context, getString(R.string.is_not_online_warning), Toast.LENGTH_SHORT).show();
            } else {
                users_list.setEnabled(false);
                sign_up_button.setEnabled(false);
                cancel_button.setEnabled(true);
                progress_bar_layout.setVisibility(View.VISIBLE);
                parsing_thread = new Thread(runnable);
                parsing_thread.start();
            }
        }
    };

    Handler progress_bar_handler, warning_message_handler, interruptionHandler;

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        HashMap<String, Object> objects = new HashMap<>();
        objects.put("thread", parsing_thread);
        objects.put("runnable", runnable);
        return objects;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList("usernames",usernames);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SQLiteDatabase.loadLibs(getApplicationContext());
        super.onCreate(savedInstanceState);
        progress_bar_handler = new Handler(getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                progressbar.incrementProgressBy(msg.what);
            }
        };
        warning_message_handler = new Handler(getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                users_list.setEnabled(true);
                sign_up_button.setEnabled(true);
                cancel_button.setEnabled(false);
                progress_bar_layout.setVisibility(View.INVISIBLE);
                progressbar.setProgress(0);
                switch (msg.what) {
                    case R.string.wrong_snils_or_password_warning:
                        Toast.makeText(context, getString(R.string.wrong_snils_or_password_warning), Toast.LENGTH_SHORT).show();
                        break;
                    case R.string.cloud_service_error_warning:
                        Toast.makeText(context, getString(R.string.cloud_service_error_warning), Toast.LENGTH_LONG).show();
                        break;
                    case R.string.parsing_problems_occured_warning:
                        Toast.makeText(context, getString(R.string.parsing_problems_occured_warning), Toast.LENGTH_LONG).show();
                        break;
                    case R.string.no_diaries_were_found:
                        Toast.makeText(context, getString(R.string.no_diaries_were_found), Toast.LENGTH_LONG).show();
                }
            }
        };
        interruptionHandler = new Handler(getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                if(parsing_was_cancelled){
                    users_list.setEnabled(true);
                    sign_up_button.setEnabled(true);
                    cancel_button.setEnabled(false);
                    progress_bar_layout.setVisibility(View.INVISIBLE);
                    progressbar.setProgress(0);
                    parsing_was_cancelled = false;
                }else {
                    Authorization_activity.super.onBackPressed();
                }
            }
        };
        setContentView(R.layout.authorization_layout);
        getSupportActionBar().setTitle(R.string.authorization_activity_header);
        context = getApplicationContext();
        USER_AGENT = getString(R.string.USER_AGENT);
        Intent intent = getIntent();
        is_first_authorization = intent.getBooleanExtra("is_first_authorization", true);
        //Getting unchangeable views
        name_edit = findViewById(R.id.name);
        login_edit = findViewById((R.id.login));
        password_edit = findViewById((R.id.password));
        sign_up_button = findViewById(R.id.sign_up_button);
        cancel_button = findViewById(R.id.cancel_button);
        progressbar = findViewById(R.id.progressBar);
        progressbar.setMax(1000);
        //Getting layout and changeable views
        LinearLayout layout = findViewById(R.id.authorization_layout);
        LinearLayout registered_users_layout = findViewById(R.id.registered_users_layout);
        progress_bar_layout = findViewById(R.id.progress_bar_layout);
        TextView second_title = findViewById(R.id.auth_layout_second_title);
        users_list = findViewById(R.id.registered_users_list);

        dbhelper = new DbHelper(context, getString(R.string.db_name),
                null, getResources().getInteger(R.integer.db_version));
        db = dbhelper.getReadableDatabase(helper_functions.getSignature(context));

        HashMap<String, Object> saved_objects  = (HashMap<String, Object>) getLastCustomNonConfigurationInstance();

        if (is_first_authorization) {
            layout.removeView(registered_users_layout);
        } else if(saved_objects == null){
            Cursor cursor = db.query(dbhelper.USER_TABLE_NAME, null, null, null, null, null, null);
            usernames = new ArrayList<>();
            snilses = new ArrayList<>();
            passwords = new ArrayList<>();
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToNext();
                passwords.add(cursor.getString(cursor.getColumnIndex(dbhelper.COLUMN_PASSWORD)));
                snilses.add(cursor.getString(cursor.getColumnIndex(dbhelper.COLUMN_SNILS)));
                usernames.add(cursor.getString(cursor.getColumnIndex(dbhelper.COLUMN_USERNAME)));
            }
            ArrayAdapter<String> users_list_adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, usernames);
            users_list.setAdapter(users_list_adapter);
            cursor.close();
        }else{
            usernames = savedInstanceState.getStringArrayList("usernames");
            ArrayAdapter<String> users_list_adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, usernames);
            users_list.setAdapter(users_list_adapter);
        }

        users_list.setOnItemClickListener(users_list_listener);
        sign_up_button.setOnClickListener(sign_up_button_listener);
        cancel_button.setOnClickListener(cancel_button_listener);

        if(saved_objects != null){
            runnable = (AuthoriozationRunnable) saved_objects.get("runnable");
            parsing_thread = (Thread) saved_objects.get("thread");
        }else{
            runnable = new AuthoriozationRunnable();
        }
        runnable.link(this);
        if(parsing_thread != null && parsing_thread.isAlive()){
            progress_bar_layout.setVisibility(View.VISIBLE);
            sign_up_button.setEnabled(false);
            users_list.setEnabled(false);
        }else{
            cancel_button.setEnabled(false);
        }

    }

    @Override
    public void onBackPressed() {
        if(is_first_authorization) {
            moveTaskToBack(true);
        }else{
            if(parsing_thread != null && parsing_thread.isAlive()) {
                parsing_thread.interrupt();
            }else{
                super.onBackPressed();
            }
        }
    }
}
