package com.sesorov.mobile_diary.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.sesorov.mobile_diary.R;
import com.sesorov.mobile_diary.helper_functions;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdSize;
import com.yandex.mobile.ads.AdView;

public class Main_menu_activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    DrawerLayout drawer_layout;
    NavigationView navigation_view;
    String username;
    String password;
    String snils;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.main_menu_header);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.main_menu_layout);
        //Initializing DrawerLayout and toggle button
        drawer_layout = findViewById(R.id.main_menu_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawer_layout,R.string.open_toggle_text,R.string.close_toggle_text);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        //Getting name, password and snils of the active user
        username = getIntent().getStringExtra("username");
        password = getIntent().getStringExtra("password");
        snils = getIntent().getStringExtra("snils");

        //Initializing widgets
        navigation_view = findViewById(R.id.navigation_view);
        navigation_view.setNavigationItemSelectedListener(this);
        View view = navigation_view.getHeaderView(0);
        LinearLayout marks_button = findViewById(R.id.Marks_button_layout);
        LinearLayout diary_button = findViewById(R.id.Diary_button_layout);
        LinearLayout notifications_button = findViewById(R.id.Notifications_button_layout);
        TextView username_textview = view.findViewById(R.id.username);
        TextView snils_textview = view.findViewById(R.id.snils);
        username_textview.setText(username);
        snils_textview.setText(snils);

        //Initializing ads
        AdView adView = findViewById(R.id.ads_view);
        adView.setBlockId("R-M-671109-1");
        adView.setAdSize(AdSize.stickySize(AdSize.FULL_WIDTH));
        final AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        diary_button.setOnClickListener(v -> {
            Intent all_weeks_for_year = new Intent(Main_menu_activity.this, All_weeks_for_year_activity.class);
            all_weeks_for_year.putExtra("password", password);
            all_weeks_for_year.putExtra("snils", snils);
            all_weeks_for_year.putExtra("mode", "online");
            startActivity(all_weeks_for_year);
        });

        marks_button.setOnClickListener(v -> {
            Intent all_marks = new Intent(Main_menu_activity.this, Marks_activity.class);
            all_marks.putExtra("password", password);
            all_marks.putExtra("snils", snils);
            all_marks.putExtra("mode", "online");
            startActivity(all_marks);
        });

        notifications_button.setOnClickListener(v -> {
            Intent settings_intent = new Intent(Main_menu_activity.this, Settings_activity.class);
            settings_intent.putExtra("password", password);
            startActivity(settings_intent);
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            if(drawer_layout.isDrawerOpen(GravityCompat.START)){
                drawer_layout.closeDrawer(GravityCompat.START);
            }else{
                drawer_layout.openDrawer(GravityCompat.START);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.exit_item:
                Intent authorization_intent = new Intent(this, Authorization_activity.class);
                authorization_intent.putExtra("is_first_authorization", false);
                startActivity(authorization_intent);
                break;
            case R.id.notifications_settings_item:
                Intent settings_intent = new Intent(this, Settings_activity.class);
                settings_intent.putExtra("password", password);
                startActivity(settings_intent);
                break;
            case R.id.privacy_policy_item:
                Intent privacy_policy_intent = new Intent(this, Privacy_policy_activity.class);
                startActivity(privacy_policy_intent);
                break;
            case R.id.about_item:
                Intent about_activity_intent = new Intent(this, About_activity.class);
                startActivity(about_activity_intent);
                break;
            case R.id.functions_description_item:
                Intent functions_description_activity_intent = new Intent(this, Functions_description_activity.class);
                startActivity(functions_description_activity_intent);
                break;
        }
        drawer_layout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


}
