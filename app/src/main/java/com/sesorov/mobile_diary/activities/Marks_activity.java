package com.sesorov.mobile_diary.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.sesorov.mobile_diary.DbHelper;
import com.sesorov.mobile_diary.JsonUser;
import com.sesorov.mobile_diary.R;
import com.sesorov.mobile_diary.helper_functions;
import com.sesorov.mobile_diary.parsers;
import com.google.gson.Gson;

import net.sqlcipher.database.SQLiteDatabase;

import java.util.LinkedHashMap;
import java.util.Map;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageButton;
import pl.droidsonroids.gif.GifImageView;

class Marks_page_adapter extends FragmentStatePagerAdapter {
    private LinkedHashMap<String,LinkedHashMap<String,LinkedHashMap<String,String>>>  all_data_from_marks_page;
    private Context context;
    Marks_page_adapter(FragmentManager fm, Context context,
                              LinkedHashMap<String,LinkedHashMap<String,LinkedHashMap<String,String>>> all_data_from_marks_page) {
        super(fm);
        this.all_data_from_marks_page = all_data_from_marks_page;
        this.context = context;
    }

    @Override
    public Fragment getItem(int i) {
        String first_lesson_name = all_data_from_marks_page.keySet().toArray(new String[0])[0];
        String title = all_data_from_marks_page.get(first_lesson_name).keySet().toArray(new String[0])[i];
        Bundle bundle = new Bundle();
        Resources res = context.getResources();
        bundle.putStringArray("old_values_array_for_replace", res.getStringArray(R.array.old_values_array_for_replace));
        bundle.putStringArray("new_values_array_for_replace", res.getStringArray(R.array.new_values_array_for_replace));
        bundle.putStringArray("headers",all_data_from_marks_page.get(first_lesson_name).get(title).keySet().toArray(new String[0]));
        bundle.putStringArray("lessons",all_data_from_marks_page.keySet().toArray(new String[0]));
        bundle.putString("title",title);
        for(String lesson:all_data_from_marks_page.keySet()){
            bundle.putStringArray(lesson,all_data_from_marks_page.get(lesson).get(title).values().toArray(new String[0]));
        }
        Marks_page_fragment fragment = new Marks_page_fragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        String first_lesson_name = all_data_from_marks_page.keySet().toArray(new String[0])[0];
        return all_data_from_marks_page.get(first_lesson_name).keySet().toArray(new String[0]).length;
    }
}

public class Marks_activity extends AppCompatActivity {
    private Map<String, String> cookies;
    private Runnable check_data_parsing;
    private final Activity activity = this;
    private Marks_page_adapter page_adapter;
    private ViewPager viewpager;
    private final Gson gson = new Gson();
    private int saved_position;
    private String mode;
    private AlertDialog alertDialog;
    public void initGifImageView() {
        GifImageButton updating_button = findViewById(R.id.updating_button);
        GifDrawable drawable = (GifDrawable) updating_button.getDrawable();
        drawable.stop();
        updating_button.setVisibility(View.VISIBLE);
        updating_button.setOnClickListener(new GifImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = "online";
                saved_position = viewpager.getCurrentItem();
                drawable.start();
                Thread thread = new Thread(check_data_parsing);
                thread.start();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SQLiteDatabase.loadLibs(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waiting_layout);
        final Intent intent = getIntent();
        final String password = intent.getStringExtra("password");
        final String snils = intent.getStringExtra("snils");
        if(savedInstanceState!=null){
            mode = savedInstanceState.getString("mode");
            saved_position = savedInstanceState.getInt("saved_position",-1);
        }else {
            mode = intent.getStringExtra("mode");
            saved_position = intent.getIntExtra("saved_position", -1);
        }
        Handler UI_handler = new Handler(getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                setContentView(R.layout.viewpager);
                TextView toolbar_header = findViewById(R.id.toolbar_header);
                toolbar_header.setText(R.string.marks_activity_header);
                int current_item_index = msg.arg1;
                page_adapter = (Marks_page_adapter) msg.obj;
                viewpager = findViewById(R.id.viewpager);
                viewpager.setAdapter(page_adapter);
                if (saved_position != -1) {
                    viewpager.setCurrentItem(saved_position);
                } else {
                    viewpager.setCurrentItem(current_item_index);
                }
                if (msg.what == 0) {
                    initGifImageView();
                }
            }
        };

        Handler dialog_handler = new Handler(getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (viewpager != null) {
                    intent.putExtra("saved_position", viewpager.getCurrentItem());
                } else {
                    intent.putExtra("saved_position", saved_position);
                }
                alertDialog = helper_functions.showAlertDialog(activity, intent, msg.what, false, mode);
            }
        };

        Runnable main_task = new Runnable() {
            @Override
            public void run() {
                LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> all_data_from_marks_page;
                if (cookies != null){
                    all_data_from_marks_page = parsers.get_all_data_from_marks_page(
                                                        getString(R.string.marks_for_the_year_url),
                                                        getString(R.string.USER_AGENT),
                                                        cookies);
                }else{
                    DbHelper dbhelper = new DbHelper(getApplicationContext(), getString(R.string.db_name),
                                            null, getResources().getInteger(R.integer.db_version));
                    SQLiteDatabase db = dbhelper.getReadableDatabase(helper_functions.getSignature(getApplicationContext()));
                    Cursor query = db.query(dbhelper.USER_TABLE_NAME, new String[]{dbhelper.COLUMN_CACHE},
                            dbhelper.COLUMN_SNILS + " = ?", new String[]{snils}, null, null, null);
                    query.moveToFirst();
                    byte [] binary_cache = query.getBlob(query.getColumnIndex(dbhelper.COLUMN_CACHE));
                    db.close();
                    query.close();
                    JsonUser json_user = gson.fromJson(new String(binary_cache), JsonUser.class);
                    all_data_from_marks_page = json_user.get_marks_activity_cache();
                }
                Marks_page_adapter page_adapter = new Marks_page_adapter(getSupportFragmentManager(), getApplicationContext(), all_data_from_marks_page);
                String[] lessons_array = all_data_from_marks_page.keySet().toArray(new String[0]);
                String[] parts_of_year = all_data_from_marks_page.get(lessons_array[0]).keySet().toArray(new String[0]);
                int i;
                for (i = parts_of_year.length-2; i >= 0; i--) {
                    boolean state = true;
                    for (int z = 1; z < lessons_array.length; z++) {
                        for (String value : all_data_from_marks_page.get(lessons_array[z]).get(parts_of_year[i]).values()) {
                            if (!value.equals("")) {
                                state = false;
                                break;
                            }
                        }
                    }
                    if (!state) {
                        break;
                    }
                }

                Message msg = new Message();
                msg.obj = page_adapter;
                msg.arg1 = i;
                msg.what = cookies != null ? 1 : 0;
                UI_handler.sendMessage(msg);
            }
        };
        check_data_parsing = new Runnable() {
            @Override
            public void run() {
                //Getting cookies
                cookies = parsers.get_cookies(getString(R.string.login_form_url), snils, password, getString(R.string.USER_AGENT));
                Message msg = new Message();

                if (!helper_functions.isOnline(getApplicationContext())) {
                    msg.what = R.string.is_not_online_warning;
                    dialog_handler.sendMessage(msg);
                } else if (cookies == null) {
                    msg.what = R.string.cloud_service_error_warning_for_dialogs;
                    dialog_handler.sendMessage(msg);
                }else if(!helper_functions.user_has_a_diary(activity, cookies)){
                    msg.what = R.string.whether_to_load_cached_data_instead_of_diary;
                    dialog_handler.sendMessage(msg);
                }else {
                    Thread thread = new Thread(main_task);
                    thread.start();
                }
            }
        };

        GifImageView gif_view = findViewById(R.id.waiting_gif);
        gif_view.postOnAnimationDelayed(new Runnable() {
            @Override
            public void run() {
                Thread thread;
                if (mode.equals("online")) {
                    thread = new Thread(check_data_parsing);
                } else {
                    thread = new Thread(main_task);
                }
                thread.start();
            }
        }, 200);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(viewpager != null) {
            saved_position = viewpager.getCurrentItem();
        }
        outState.putInt("saved_position", saved_position);
        outState.putString("mode", mode);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        if(alertDialog != null){
            alertDialog.dismiss();
        }
        super.onDestroy();
    }
}