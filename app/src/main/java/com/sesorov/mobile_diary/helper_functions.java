package com.sesorov.mobile_diary;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.sesorov.mobile_diary.activities.All_weeks_for_year_activity;
import com.sesorov.mobile_diary.activities.Marks_activity;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteException;

import org.apache.commons.collections4.ListUtils;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Handler;

public class helper_functions {

    public static void Create_notification(Context context, Intent notificationIntent, String title, String text, int notify_id) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        Resources res = context.getResources();
        String Channel_id = context.getString(R.string.notifications_channel_id);
        //necessary for supporting low API levels
        Bitmap originLargeIcon = BitmapFactory.decodeResource(res, R.drawable.ic_launcher);
        int width = (int) res.getDimension(android.R.dimen.notification_large_icon_width);
        int height = (int) res.getDimension(android.R.dimen.notification_large_icon_height);
        //filter = true is necessary for supporting Xiaomi devices(it also improves quality of the image)
        Bitmap scaledLargeIcon = Bitmap.createScaledBitmap(originLargeIcon, width, height, true);
        //Configuring builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Channel_id);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.small_notification_icon)
                .setLargeIcon(scaledLargeIcon)
                .setContentTitle(title)
                .setContentText(text)
                .setStyle(new NotificationCompat.BigTextStyle()
                         .bigText(text))
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(Channel_id, Channel_id, NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(notify_id, builder.build());
    }
    public static String Read_byte_file(String name,Context context){
        String text = null;
        try {
            FileInputStream reader = context.openFileInput(name);
            byte[] array = new byte[reader.available()];
            reader.read(array);
            text = new String(array);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static boolean file_exists(String name,Context context) {
        try {
            FileInputStream open_file = context.openFileInput(name);
            byte[] bytes = new byte[open_file.available()];
            open_file.read(bytes);
            String text = new String(bytes);
            if (text.equals("") | text.equals(" ")){
                return false;
            }else{
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getFormattedDate(Date date){
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        return format.format(date);
    }

    public static Date get_date_from_string(String date_string){
        int year;
        int month;
        int day;
        String[]date_array = date_string.split("[.\\-]");
        if(date_array[2].length()>date_array[0].length()){
            year = Integer.parseInt(date_array[2]);
            day = Integer.parseInt(date_array[0]);
        }else{
            year = Integer.parseInt(date_array[0]);
            day = Integer.parseInt(date_array[2]);
        }
        month = Integer.parseInt(date_array[1])-1;

        Calendar calendar = get_calendar_without_time_of_day();

        calendar.set(year,month,day);

        calendar.getTime();
        return calendar.getTime();
    }

    public static void setCalendar(Calendar calendar,String date_string){


        String[] date = date_string.split("\\.");

        int day = Integer.parseInt(date[0]);
        int month = Integer.parseInt(date[1]) - 1;
        int year = Integer.parseInt(date[2]);

        calendar.set(year,month,day);
        calendar.getTime();
    }

    public static Date getLastDayOfWeek(String date_string,int last_day_of_week){
        Calendar calendar = get_calendar_without_time_of_day();

        String[] date = date_string.split("\\.");

        int day = Integer.parseInt(date[0]);
        int month = Integer.parseInt(date[1]) - 1;
        int year = Integer.parseInt(date[2]);

        calendar.set(year,month,day);
        calendar.getTime();
        calendar.set(Calendar.DAY_OF_WEEK,last_day_of_week);

        return calendar.getTime();
    }

   public static long getWeekNumber(Date date){
        Calendar calendar = get_calendar_without_time_of_day();
        calendar.setTime(date);
        calendar.getTime();
        return calendar.get(Calendar.WEEK_OF_YEAR);
   }

   public static ArrayList<HashMap<String,String>> get_homework_for_weeks(Context context, Map<String,String> cookies, int weeks) {

        Calendar calendar = get_calendar_without_time_of_day();

        while(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY){
            calendar.add(Calendar.DAY_OF_WEEK,-1);
        }


       int week_number = calendar.get(Calendar.WEEK_OF_YEAR);

        String[] splitted_action_url = context.getString(R.string.front_diary_page_url).split("data");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        String USER_AGENT = context.getString(R.string.USER_AGENT);

        List<HashMap<String, String>> res = new ArrayList<>();

        for(int i = 0;i < weeks;i++) {
            calendar.set(Calendar.WEEK_OF_YEAR,week_number);
            calendar.getTime();

            String login_action_url = splitted_action_url[0] + format.format(calendar.getTime());
            List<HashMap<String, String>> week_homework = parsers.get_week_data(login_action_url,USER_AGENT,cookies);
            if(week_homework==null){
                return null;
            }

            res = ListUtils.union(res, week_homework);

            week_number++;
        }
        return (ArrayList<HashMap<String,String>>)res;
   }

    public static ArrayList<HashMap<String,String>> get_comments_for_weeks(Context context, Map<String,String> cookies, int weeks) {

        Calendar calendar = get_calendar_without_time_of_day();

        while(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY){
            calendar.add(Calendar.DAY_OF_WEEK,-1);
        }


        int week_number = calendar.get(Calendar.WEEK_OF_YEAR);

        String[] splitted_action_url = context.getString(R.string.front_diary_page_url).split("data");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        String USER_AGENT = context.getString(R.string.USER_AGENT);

        List<HashMap<String, String>> res = new ArrayList<>();

        for(int i = 0;i < weeks;i++) {
            calendar.set(Calendar.WEEK_OF_YEAR,week_number);
            calendar.getTime();

            String login_action_url = splitted_action_url[0] + format.format(calendar.getTime());

            List<HashMap<String, String>> week_comments = parsers.get_week_comments(login_action_url,USER_AGENT,cookies);
            if(week_comments==null){
                return null;
            }
            res = ListUtils.union(res, week_comments);

            week_number++;
        }
        return (ArrayList<HashMap<String,String>>)res;
    }

   public static Calendar get_calendar_without_time_of_day(){
        Calendar calendar = Calendar.getInstance();
        //TODO:проверить, нормально ли все работает при переходе из одного часового пояса в другой
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        //it is impossible to clear the HOUR_OF_DAY, HOUR and AM_PM fields
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
        calendar.getTime();
        return calendar;
   }

   public static boolean isOnline(Context context){
       final String correct_signature = "jDkIaRaQacZmrJtHHD0kSGfrWwQ=";
       String apkSignature = null;
       try {
           PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
                   context.getPackageName(),
                   PackageManager.GET_SIGNATURES
           );

           for (Signature signature : packageInfo.signatures) {
               MessageDigest md = MessageDigest.getInstance("SHA-1");
               md.update(signature.toByteArray());
               apkSignature = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
           }
       } catch (NoSuchAlgorithmException | PackageManager.NameNotFoundException e) {
           e.printStackTrace();
       }
       if(!apkSignature.equals(correct_signature)) {
           try {
               FileOutputStream stream = new FileOutputStream(context.getDatabasePath(context.getString(R.string.db_name)));
               stream.write(new byte[]{1,1,1,1,1});
               stream.close();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       ConnectivityManager manager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
       NetworkInfo network_info = manager.getActiveNetworkInfo();
       if(network_info != null){
           return true;
       }
       return false;
   }

   public static boolean check_string_condition(int value, String condition){
        if(condition.equals("0")){
            return false;
        }
        switch(condition.substring(0,2)){
            case ">=":
                return value >= condition.charAt(2);
            case "<=":
                return value <= condition.charAt(2);
            default:
                return false;
        }
   }

   public static String getSignature(Context context){
       String apkSignature = null;
       try {
           PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
                   context.getPackageName(),
                   PackageManager.GET_SIGNATURES
           );
           for (Signature signature : packageInfo.signatures) {
               MessageDigest md = MessageDigest.getInstance("SHA-256");
               md.update(signature.toByteArray());
               apkSignature = Base64.encodeToString(md.digest(), Base64.DEFAULT);
           }
       } catch (Exception e) {}

       return apkSignature;
   }

   public static Date get_monday_from_page(Document page) throws IllegalArgumentException{
       Element title = page.getElementsByClass("diary-page-title").first();
       if(title == null){
           throw new IllegalArgumentException("No title on the page!");
       }
       return get_date_from_string(title.text().split(" ")[2]);
   }

   public static boolean user_has_a_diary(Context context, Map<String, String> cookies){
       Document current_week_page;
       try {
           current_week_page = Jsoup.connect(context.getString(R.string.default_page_url))
                   .cookies(cookies)
                   .userAgent(context.getString(R.string.USER_AGENT))
                   .get();
       } catch (IOException e) {
           e.printStackTrace();
           if(e.getClass() == InterruptedIOException.class){
               Thread.currentThread().interrupt();
           }
           return false;
       }
       Element title = current_week_page.getElementsByClass("diary-page-title").first();
       return title != null;
   }

//   public static Calendar get_first_week_monday(Context context, int range,
//                                                Map<String, String> cookies){
//       Calendar calendar = helper_functions.get_calendar_without_time_of_day();
//       calendar.set(calendar.get(Calendar.YEAR) + range, 6, 1);
//
//       String[] splitted_action_url = context.getString(R.string.front_diary_page_url).split("data");
//       DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
//       String login_action_url = splitted_action_url[0] + format.format(calendar.getTime());
//       Document page;
//       try {
//           page = Jsoup.connect(login_action_url)
//                   .cookies(cookies)
//                   .userAgent(context.getString(R.string.USER_AGENT))
//                   .get();
//       } catch (IOException e) {
//           e.printStackTrace();
//           return null;
//       }
//       Calendar output_calendar = get_calendar_without_time_of_day();
//       output_calendar.setTime(get_monday_from_page(page));
//       return output_calendar;
//   }
//
//    public static Calendar get_last_week_monday(Context context, int range,
//                                                Map<String, String> cookies){
//        Calendar calendar = helper_functions.get_calendar_without_time_of_day();
//        calendar.set(calendar.get(Calendar.YEAR) + range, 6, 1);
//
//        String[] splitted_action_url = context.getString(R.string.front_diary_page_url).split("data");
//        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
//        String login_action_url = splitted_action_url[0] + format.format(calendar.getTime());
//        Document page;
//        try {
//            page = Jsoup.connect(login_action_url)
//                    .cookies(cookies)
//                    .userAgent(context.getString(R.string.USER_AGENT))
//                    .get();
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//        Calendar output_calendar = get_calendar_without_time_of_day();
//        output_calendar.setTime(get_monday_from_page(page));
//        return output_calendar;
//    }

    public static Calendar[] get_first_and_last_week_mondays(Context context, Map<String, String> cookies)
                                                            throws IllegalArgumentException{
        Document current_week_page;
        try {
            current_week_page = Jsoup.connect(context.getString(R.string.default_page_url))
                    .cookies(cookies)
                    .userAgent(context.getString(R.string.USER_AGENT))
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
            if(e.getClass() == InterruptedIOException.class){
                Thread.currentThread().interrupt();
            }
            return null;
        }

        Calendar current_week_monday = helper_functions.get_calendar_without_time_of_day();
        current_week_monday.setTime(get_monday_from_page(current_week_page));
        byte[] offsets = new byte[2];
        if (current_week_monday.get(Calendar.MONTH) < 6) {
            offsets[0] = -1;
        } else {
            offsets[1] = 1;
        }

        Calendar[] calendars = new Calendar[2];
        for(int i = 0; i < 2; i++) {
            Calendar calendar = helper_functions.get_calendar_without_time_of_day();
            calendar.set(calendar.get(Calendar.YEAR) + offsets[i], 6, 1);

            String[] splitted_action_url = context.getString(R.string.front_diary_page_url).split("data");
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String login_action_url = splitted_action_url[0] + format.format(calendar.getTime());
            Document page;
            try {
                page = Jsoup.connect(login_action_url)
                        .cookies(cookies)
                        .userAgent(context.getString(R.string.USER_AGENT))
                        .get();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            Calendar output_calendar = get_calendar_without_time_of_day();
            output_calendar.setTime(get_monday_from_page(page));
            calendars[i] = output_calendar;
        }
        return calendars;
    }

   public static void set_user_cache(SQLiteDatabase db, Context context, @Nullable JsonUser json_user,
                                     @NonNull Map<String, String> cookies, long user_id, Resources resources,
                                     @NonNull Calendar calendar, @NonNull Calendar calendar2){
        if(json_user==null){
            json_user = new JsonUser();
        }
       String USER_AGENT = resources.getString(R.string.USER_AGENT);
       Gson gson = new Gson();
       HashMap<String, ArrayList<HashMap<String, String>>> all_weeks_activity_cache = new HashMap<>();

       //Getting all_weeks_activity_cache
       Document current_week_page = null;
       try {
           current_week_page = Jsoup.connect(context.getString(R.string.default_page_url))
                   .cookies(cookies)
                   .userAgent(context.getString(R.string.USER_AGENT))
                   .get();
       } catch (IOException e) {
           e.printStackTrace();
       }

       Calendar current_week_monday_from_cloud = helper_functions.get_calendar_without_time_of_day();
       current_week_monday_from_cloud.setTime(helper_functions.get_monday_from_page(current_week_page));
       Date first_day_of_week = current_week_monday_from_cloud.getTime();

       DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
       String[] splitted_action_url = resources.getString(R.string.front_diary_page_url).split("data");
       String login_action_url = splitted_action_url[0] + format.format(first_day_of_week);
       ArrayList<HashMap<String, String>>  data_for_week = parsers.get_full_week_data(login_action_url,USER_AGENT,cookies);
       int weeks_number = resources.getInteger(R.integer.cache_storing_limit_in_weeks);
       //Getting all_weeks_activity_cache
       boolean is_interrupted = false;
       all_weeks_activity_cache.put(first_day_of_week.toString(),data_for_week);
       for(int i = 0;i < weeks_number;i++){
           if(is_interrupted){
               break;
           }
           current_week_monday_from_cloud.add(Calendar.WEEK_OF_YEAR, 1);
           first_day_of_week = current_week_monday_from_cloud.getTime();
           login_action_url = splitted_action_url[0] + format.format(first_day_of_week);
           data_for_week = parsers.get_full_week_data(login_action_url,USER_AGENT,cookies);
           if(data_for_week==null){
               is_interrupted = true;
           }
           all_weeks_activity_cache.put(first_day_of_week.toString(),data_for_week);
       }

       current_week_monday_from_cloud.add(Calendar.WEEK_OF_YEAR, -weeks_number);

       for(int i = 0;i < weeks_number;i++){
           if(is_interrupted){
               break;
           }
           current_week_monday_from_cloud.add(Calendar.WEEK_OF_YEAR, -1);
           first_day_of_week = current_week_monday_from_cloud.getTime();
           login_action_url = splitted_action_url[0] + format.format(first_day_of_week);
           data_for_week = parsers.get_full_week_data(login_action_url,USER_AGENT,cookies);
           if(data_for_week==null){
               is_interrupted = true;
           }
           all_weeks_activity_cache.put(first_day_of_week.toString(),data_for_week);
       }
       if(!is_interrupted){
           json_user.set_all_weeks_activity_cache(all_weeks_activity_cache);
           //Getting mondays
           HashMap<String, ArrayList<Date>> mondays_of_school_year_weeks_cache = new HashMap<>();
           ArrayList<Date> date_when_updated = new ArrayList<>();
           Calendar current_calendar = helper_functions.get_calendar_without_time_of_day();
           date_when_updated.add(current_calendar.getTime());
           mondays_of_school_year_weeks_cache.put(context.getString(R.string.date_when_was_updated), date_when_updated);

           ArrayList<Date> mondays = new ArrayList<>();
           while (calendar.before(calendar2) || calendar.equals(calendar2)) {
               mondays.add(calendar.getTime());
               calendar.add(Calendar.WEEK_OF_YEAR, 1);
           }
           mondays_of_school_year_weeks_cache.put(context.getString(R.string.mondays), mondays);
           json_user.set_mondays_of_school_year_weeks_cache(mondays_of_school_year_weeks_cache);
       }
       //Getting marks_activity_cache
       LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> marks_activity_cache =
               parsers.get_all_data_from_marks_page(
                       resources.getString(R.string.marks_for_the_year_url),
                       resources.getString(R.string.USER_AGENT),
                       cookies);
       if(marks_activity_cache!=null){
           json_user.set_marks_activity_cache(marks_activity_cache);
       }
       byte[] binary_cache = gson.toJson(json_user).getBytes();
       ContentValues values = new ContentValues();
       values.put(DbHelper.COLUMN_CACHE, binary_cache);
//     SqliteException occurs there sometimes. I don't know the cause of it.
       while(true){
           try {
               db.update(DbHelper.USER_TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(user_id)});
               break;
           }catch (SQLiteException e){
               e.printStackTrace();
           }
       }
   }

   public static AlertDialog showAlertDialog(Activity activity, Intent intent, int message,
                                      boolean wifiWasTurnedOff, String mode){ // for All_weeks_for_year_activity and Marks_activity only
        AlertDialog alertDialog =  new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(R.string.switch_to_offline_mode_text, (dialog, which) -> {
                    if(wifiWasTurnedOff||mode.equals("offline")){
                        try {
                            ((All_weeks_for_year_activity) activity).initGifImageView();
                        }catch (ClassCastException e){
                            e.printStackTrace();
                            ((Marks_activity) activity).initGifImageView();
                        }
                    }
                    if(mode.equals("online")&&!wifiWasTurnedOff){
                        intent.putExtra("mode", "offline");
                        activity.finish();
                        activity.startActivity(intent);
                    }
                })
                .setNegativeButton(activity.getString(R.string.try_again_text), (dialog, which) -> {
                    intent.putExtra("mode", "online");
                    activity.finish();
                    activity.startActivity(intent);
                })
                .setOnCancelListener(dialogInterface -> activity.onBackPressed())
                .show();
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }
}
