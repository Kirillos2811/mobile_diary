package com.sesorov.mobile_diary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import androidx.annotation.Nullable;

import com.google.gson.Gson;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

public  class DbHelper extends SQLiteOpenHelper {
    //Tables
    public static final String USER_TABLE_NAME = "USER_TABLE";
    public static final String ALL_HOMEWORK_TABLE_NAME = "ALL_HOMEWORK_TABLE";
    public static final String ALL_MARKS_TABLE_NAME = "ALL_MARKS_TABLE";
    public static final String ALL_COMMENTS_TABLE_NAME = "ALL_COMMENTS_TABLE";
    public static final String NOTIFICATIONS_TABLE_NAME = "NOTIFICATIONS_TABLE";
    //Columns
    public static final String COLUMN_DATE = "DATE";
    public static final String COLUMN_WEEK_NUMBER = "WEEK_NUMBER";
    public static final String COLUMN_ACTIVE_STATUS = "isActive";
    public static final String COLUMN_IS_DONE = "isDone";
    public static final String COLUMN_LESSON = "LESSON";
    public static final String COLUMN_HOMEWORK = "HOMEWORK";
    public static final String COLUMN_MARK = "MARK";
    public static final String COLUMN_COMMENT = "COMMENT";
    public static final String COLUMN_USERNAME = "USERNAME";
    public static final String COLUMN_SNILS = "SNILS";
    public static final String COLUMN_PASSWORD = "PASSWORD";
    public static final String COLUMN_USER_ID_REFERENCE = "user_id";
    public static final String COLUMN_DATE_WHEN_ADDED = "DATE_WHEN_ADDED";
    public static final String COLUMN_TITLE = "TITLE";
    public static final String COLUMN_TEXT = "TEXT";
    public static final String COLUMN_NEW_STATUS = "isNew";
    public static final String COLUMN_MARK_NOTIFICATIONS_SETTING = "MARK_NOTIFICATIONS_SETTING";
    public static final String COLUMN_HOMEWORK_NOTIFICATIONS_SETTING = "HOMEWORK_NOTIFICATIONS_SETTING";
    public static final String COLUMN_COMMENT_NOTIFICATIONS_SETTING = "COMMENT_NOTIFICATIONS_SETTING";
    public static final String COLUMN_CACHE = "CACHE";

    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    public long insertUser(SQLiteDatabase db, String username, String snils, String password,
                           int isActive, String mark_notification_setting, String homework_notification_setting,
                           String comment_notification_setting){
        Gson gson = new Gson();
        ContentValues new_values = new ContentValues();
        new_values.put(COLUMN_USERNAME,username);
        new_values.put(COLUMN_SNILS,snils);
        new_values.put(COLUMN_PASSWORD,password);
        new_values.put(COLUMN_ACTIVE_STATUS,isActive);
        new_values.put(COLUMN_MARK_NOTIFICATIONS_SETTING, mark_notification_setting);
        new_values.put(COLUMN_HOMEWORK_NOTIFICATIONS_SETTING, homework_notification_setting);
        new_values.put(COLUMN_COMMENT_NOTIFICATIONS_SETTING, comment_notification_setting);
        new_values.put(COLUMN_CACHE, gson.toJson(new JsonUser()).getBytes());
        long user_id = db.insert(USER_TABLE_NAME, null, new_values);
        new_values.clear();
        return user_id;
    }

    public  void delete_user_data(SQLiteDatabase db, long user_id){
        db.delete(ALL_MARKS_TABLE_NAME, COLUMN_USER_ID_REFERENCE + " = ?",
                new String[]{String.valueOf(user_id)});
        db.delete(ALL_HOMEWORK_TABLE_NAME, COLUMN_USER_ID_REFERENCE + " = ?",
                new String[]{String.valueOf(user_id)});
        db.delete(ALL_COMMENTS_TABLE_NAME, COLUMN_USER_ID_REFERENCE + " = ?",
                new String[]{String.valueOf(user_id)});
        db.delete(USER_TABLE_NAME,"_id = ?", new String[]{String.valueOf(user_id)});
    }

    public long insertHomework(SQLiteDatabase db, long date,long week_number,String lesson,String homework, int isDone, long user_id) {

        ContentValues new_values = new ContentValues();
        new_values.put(COLUMN_DATE,date);
        new_values.put(COLUMN_WEEK_NUMBER,week_number);
        new_values.put(COLUMN_LESSON,lesson);
        new_values.put(COLUMN_HOMEWORK,homework);
        new_values.put(COLUMN_IS_DONE,isDone);
        new_values.put(COLUMN_USER_ID_REFERENCE,user_id);
        long id = db.insert(ALL_HOMEWORK_TABLE_NAME,null,new_values);
        new_values.clear();
        return id;
    }

    public long insertMark(SQLiteDatabase db,long date,long week_number, String lesson,String mark,long user_id) {
        ContentValues new_values = new ContentValues();
        new_values.put(COLUMN_DATE,date);
        new_values.put(COLUMN_WEEK_NUMBER,week_number);
        new_values.put(COLUMN_LESSON,lesson);
        new_values.put(COLUMN_MARK,mark);
        new_values.put(COLUMN_USER_ID_REFERENCE,user_id);
        long id = db.insert(ALL_MARKS_TABLE_NAME,null,new_values);
        new_values.clear();
        return id;
    }

    public long insertComment(SQLiteDatabase db,long date, @Nullable String lesson, long week_number, String comment,long user_id) {
        ContentValues new_values = new ContentValues();
        new_values.put(COLUMN_DATE,date);
        new_values.put(COLUMN_LESSON,lesson);
        new_values.put(COLUMN_WEEK_NUMBER,week_number);
        new_values.put(COLUMN_COMMENT,comment);
        new_values.put(COLUMN_USER_ID_REFERENCE,user_id);
        long id = db.insert(ALL_COMMENTS_TABLE_NAME,null,new_values);
        new_values.clear();
        return id;
    }

    public Cursor getActiveUser(SQLiteDatabase db){
        Cursor user_query  = db.query(USER_TABLE_NAME,
                null,
                COLUMN_ACTIVE_STATUS+" = ?",
                new String[]{"1"},
                null,
                null,
                null);
        user_query.moveToFirst();
        return user_query;
    }

    public boolean insert_homework_if_not_exists(SQLiteDatabase db, long date,long week_number,String lesson,String homework, int isDone, long user_id){
        Cursor query = db.query(ALL_HOMEWORK_TABLE_NAME,
                null,
                COLUMN_USER_ID_REFERENCE + " = ? "  + "and " +  COLUMN_DATE + " = ? "
                        + "and " + COLUMN_LESSON + " = ? " + "and " + COLUMN_HOMEWORK + " = ?",
                new String[]{String.valueOf(user_id), String.valueOf(date), lesson, homework},
                null,
                null,
                null);
        if (query.getCount() == 0){
            insertHomework(db,date,week_number,lesson,homework,isDone,user_id);
            query.close();
            return true;
        }else{
            query.close();
            return false;
        }
    }

    public boolean insert_mark_if_not_exists(SQLiteDatabase db,long date,long week_number, String lesson,String mark,long user_id){
        Cursor query = db.query(ALL_MARKS_TABLE_NAME,
                null,
                COLUMN_USER_ID_REFERENCE + " = ? "  + "and " +  COLUMN_DATE+" = ? "
                        + "and " + COLUMN_LESSON + " = ? " + "and " + COLUMN_MARK+" = ?",
                new String[]{String.valueOf(user_id), String.valueOf(date), lesson, mark},
                null,
                null,
                null);
        if (query.getCount() == 0){
            insertMark(db,date,week_number,lesson,mark,user_id);
            query.close();
            return true;
        }else{
            query.close();
            return false;
        }
    }

    public boolean insert_comment_if_not_exists(SQLiteDatabase db,long date, String lesson, long week_number, String comment,long user_id){
        Cursor query = db.query(ALL_COMMENTS_TABLE_NAME,
                null,
                COLUMN_USER_ID_REFERENCE + " = ? "  + "and " +  COLUMN_DATE + " = ? "//TODO:must check lesson!
                        + "and " + COLUMN_COMMENT + " = ?",
                new String[]{String.valueOf(user_id), String.valueOf(date), comment},
                null,
                null,
                null);
        if (query.getCount() == 0){
            insertComment(db,date, lesson, week_number,comment,user_id);
            query.close();
            return true;
        }else{
            query.close();
            return false;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + USER_TABLE_NAME +"("+
                "_id INTEGER PRIMARY KEY, "+
                 COLUMN_USERNAME + " TEXT NOT NULL,"+
                 COLUMN_SNILS + " TEXT NOT NULL,"+
                 COLUMN_PASSWORD + " TEXT NOT NULL,"+
                 COLUMN_ACTIVE_STATUS + " INTEGER NOT NULL," +
                 COLUMN_MARK_NOTIFICATIONS_SETTING + " TEXT NOT NULL," +
                 COLUMN_HOMEWORK_NOTIFICATIONS_SETTING + " TEXT NOT NULL," +
                 COLUMN_COMMENT_NOTIFICATIONS_SETTING + " TEXT NOT NULL," +
                 COLUMN_CACHE + " BLOB NOT NULL);");

        db.execSQL("CREATE TABLE " + ALL_HOMEWORK_TABLE_NAME +"("+
                "_id INTEGER PRIMARY KEY, "+
                 COLUMN_DATE + " INTEGER NOT NULL,"+
                 COLUMN_WEEK_NUMBER + " INTEGER NOT NULL,"+
                 COLUMN_LESSON + " TEXT NOT NULL," +
                 COLUMN_HOMEWORK + " TEXT NOT NULL," +
                 COLUMN_IS_DONE + " INTEGER NOT NULL," +
                 COLUMN_USER_ID_REFERENCE  + " INTEGER NOT NULL," +
                "FOREIGN KEY (user_id) REFERENCES "+ USER_TABLE_NAME + "(_id)  );");

        db.execSQL("CREATE TABLE " + ALL_MARKS_TABLE_NAME +"("+
                "_id INTEGER PRIMARY KEY, "+
                 COLUMN_DATE + " INTEGER NOT NULL,"+
                 COLUMN_WEEK_NUMBER + " INTEGER NOT NULL,"+
                 COLUMN_LESSON + " TEXT NOT NULL," +
                 COLUMN_MARK + " TEXT NOT NULL," +
                 COLUMN_USER_ID_REFERENCE  + " INTEGER NOT NULL," +
                "FOREIGN KEY (user_id) REFERENCES "+ USER_TABLE_NAME + "(_id)  );");

        db.execSQL("CREATE TABLE " + ALL_COMMENTS_TABLE_NAME +"("+
                "_id INTEGER PRIMARY KEY, "+
                 COLUMN_DATE + " INTEGER NOT NULL,"+
                 COLUMN_LESSON + " TEXT," +
                 COLUMN_WEEK_NUMBER + " INTEGER NOT NULL,"+
                 COLUMN_COMMENT + " TEXT NOT NULL," +
                 COLUMN_USER_ID_REFERENCE  + " INTEGER NOT NULL," +
                "FOREIGN KEY (user_id) REFERENCES "+ USER_TABLE_NAME + "(_id)  );");

        db.execSQL("CREATE TABLE " + NOTIFICATIONS_TABLE_NAME +"("+
                "_id INTEGER PRIMARY KEY, "+
                 COLUMN_DATE_WHEN_ADDED + " INTEGER NOT NULL,"+
                 COLUMN_WEEK_NUMBER + " INTEGER NOT NULL,"+
                 COLUMN_TITLE + " TEXT NOT NULL," +
                 COLUMN_TEXT + " TEXT NOT NULL," +
                 COLUMN_NEW_STATUS + " INTEGER NOT NULL," +
                 COLUMN_USER_ID_REFERENCE  + " INTEGER NOT NULL," +
                "FOREIGN KEY (user_id) REFERENCES "+ USER_TABLE_NAME + "(_id)  );");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ALL_HOMEWORK_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ALL_MARKS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ALL_COMMENTS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NOTIFICATIONS_TABLE_NAME);

        onCreate(db);
    }
}
