package com.sesorov.mobile_diary;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class JsonUser {
    private HashMap<String, ArrayList<HashMap<String, String>>> all_weeks_activity_cache = null;
    private LinkedHashMap<String,LinkedHashMap<String,LinkedHashMap<String,String>>> marks_activity_cache = null;
    private HashMap<String, ArrayList<Date>> mondays_of_school_year_weeks_cache = new HashMap<>();
    public void set_mondays_of_school_year_weeks_cache(HashMap<String, ArrayList<Date>> mondays_of_school_year_weeks_cache){
        this.mondays_of_school_year_weeks_cache = mondays_of_school_year_weeks_cache;
    }
    public HashMap<String, ArrayList<Date>> get_mondays_of_school_year_weeks_cache(){
        return mondays_of_school_year_weeks_cache;
    }
    public void set_all_weeks_activity_cache(HashMap<String, ArrayList<HashMap<String, String>>> all_weeks_activity_cache){
        this.all_weeks_activity_cache = all_weeks_activity_cache;
    }
    public void set_marks_activity_cache(LinkedHashMap<String,LinkedHashMap<String,LinkedHashMap<String,String>>> marks_activity_cache ){
        this.marks_activity_cache = marks_activity_cache;
    }
    public HashMap<String, ArrayList<HashMap<String, String>>>  get_all_weeks_activity_cache(){
        return all_weeks_activity_cache;
    }
    public LinkedHashMap<String,LinkedHashMap<String,LinkedHashMap<String,String>>> get_marks_activity_cache(){
        return marks_activity_cache;
    }
}
