package com.sesorov.mobile_diary;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.sesorov.mobile_diary.activities.SplashScreenActivity;
import com.splunk.mint.Mint;

import net.sqlcipher.database.SQLiteDatabase;

import org.apache.commons.collections4.ListUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class BR_Receiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Mint.initAndStartSession(context, "b1da1e8c");
        Resources res = context.getResources();
        SQLiteDatabase.loadLibs(context);
        DbHelper dbhelper = new DbHelper(context, res.getString(R.string.db_name), null, res.getInteger(R.integer.db_version));
        SQLiteDatabase db = dbhelper.getReadableDatabase(helper_functions.getSignature(context));
        Cursor cursor = db.query(dbhelper.USER_TABLE_NAME, null, null, null, null, null, null);
        String USER_AGENT = res.getString(R.string.USER_AGENT);
        Gson gson = new Gson();
        Runnable runnable = () -> {
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(dbhelper.COLUMN_USERNAME));
                String password = cursor.getString(cursor.getColumnIndex(dbhelper.COLUMN_PASSWORD));
                String snils = cursor.getString(cursor.getColumnIndex(dbhelper.COLUMN_SNILS));
                String user_mark_notification_setting = cursor.getString(cursor.getColumnIndex(dbhelper.COLUMN_MARK_NOTIFICATIONS_SETTING));
                String user_homework_notification_setting = cursor.getString(cursor.getColumnIndex(dbhelper.COLUMN_HOMEWORK_NOTIFICATIONS_SETTING));
                String user_comment_notification_setting = cursor.getString(cursor.getColumnIndex(dbhelper.COLUMN_COMMENT_NOTIFICATIONS_SETTING));
                byte[] binary_cache = cursor.getBlob(cursor.getColumnIndex(dbhelper.COLUMN_CACHE));
                JsonUser json_user = gson.fromJson(new String(binary_cache),JsonUser.class);
                long user_id = cursor.getLong(cursor.getColumnIndex("_id"));
                final Intent notification_intent = new Intent(context, SplashScreenActivity.class);

                //Getting cookies
                Map<String, String> cookies = parsers.get_cookies(res.getString(R.string.login_form_url), snils,
                        password, USER_AGENT);
                if(cookies != null) {
                    Calendar[] first_and_last_mondays_of_the_school_year = null;
                    try {
                        first_and_last_mondays_of_the_school_year = helper_functions.
                                get_first_and_last_week_mondays(context, cookies);
                    }catch (IllegalArgumentException e){
                        e.printStackTrace();
                    }
                    if(first_and_last_mondays_of_the_school_year != null) {
                        Calendar calendar = first_and_last_mondays_of_the_school_year[0];
                        Calendar calendar2 = first_and_last_mondays_of_the_school_year[1];
                        helper_functions.set_user_cache(db, context, json_user, cookies, user_id, res, calendar, calendar2);

                        //Getting homework for weeks
                        ArrayList<HashMap<String, String>> all_homework = helper_functions.get_homework_for_weeks(context, cookies, 2);
                        //Getting marks
                        ArrayList<HashMap<String, String>> all_marks_from_cloud = parsers.get_all_marks(
                                res.getString(R.string.marks_for_the_year_url),
                                res.getString(R.string.USER_AGENT),
                                cookies);
                        //Getting comments from comments_page
                        ArrayList<HashMap<String, String>> comments_from_comments_page = parsers.get_all_comments(
                                res.getString(R.string.all_comments_url),
                                res.getString(R.string.USER_AGENT),
                                cookies);
                        //Getting comments for weeks
                        ArrayList<HashMap<String, String>> comments_for_weeks = helper_functions.get_comments_for_weeks(context, cookies, 2);

                        if (all_homework != null) {
                            //Pushing new homework into DB
                            for (HashMap<String, String> item : all_homework) {
                                Date date = helper_functions.get_date_from_string(item.get("DATE"));
                                boolean new_status = dbhelper.insert_homework_if_not_exists(db, date.getTime(),
                                        helper_functions.getWeekNumber(date), item.get("LESSON"),
                                        item.get("HOMEWORK"), 1, user_id);
                                //Adding notification about new_homework to the list
                                if (new_status & user_homework_notification_setting.equals("1")) {
                                    String text = res.getString(R.string.text_for_homework_notifications);
                                    text = text.replace("name", name);
                                    text = text.replace("lesson", item.get("LESSON"));
                                    helper_functions.Create_notification(context, notification_intent,
                                            res.getString(R.string.title_for_homework_notifications),
                                            text, (int) System.currentTimeMillis());
                                }
                            }
                        }

                        if (all_marks_from_cloud != null) {
                            //Pushing new marks into DB
                            ArrayList<HashMap<String, String>> all_marks_from_db = new ArrayList<>();
                            Cursor marks_query = db.query(DbHelper.ALL_MARKS_TABLE_NAME,
                                    null,
                                    DbHelper.COLUMN_USER_ID_REFERENCE + " = ?",
                                    new String[]{String.valueOf(user_id)},
                                    null,
                                    null,
                                    null);
                            DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                            while (marks_query.moveToNext()) {
                                HashMap<String, String> mark = new HashMap<>();
                                Date date = new Date(marks_query.getLong(marks_query.getColumnIndex(DbHelper.COLUMN_DATE)));
                                mark.put("DATE", format.format(date));
                                mark.put("LESSON", marks_query.getString(marks_query.getColumnIndex(DbHelper.COLUMN_LESSON)));
                                mark.put("MARK", marks_query.getString(marks_query.getColumnIndex(DbHelper.COLUMN_MARK)));
                                all_marks_from_db.add(mark);
                            }

                            List<HashMap<String, String>> new_marks = ListUtils.subtract(all_marks_from_cloud, all_marks_from_db);

                            for (HashMap<String, String> item : new_marks ) {
                                Date date = helper_functions.get_date_from_string(item.get("DATE"));
                                dbhelper.insertMark(db, date.getTime(),
                                        helper_functions.getWeekNumber(date), item.get("LESSON"), item.get("MARK"), user_id);
                                //Adding notification about new_mark to the list
                                for (int i = 0; i < item.get("MARK").length(); i++) {
                                    char char_mark = item.get("MARK").charAt(i);
                                    if (Character.isDigit(item.get("MARK").charAt(i)) &
                                            helper_functions.check_string_condition(char_mark, user_mark_notification_setting)) {
                                        String text = res.getString(R.string.text_for_mark_notifications);
                                        text = text.replace("name", name);
                                        text = text.replace("lesson", item.get("LESSON"));
                                        text = text.replace("mark", item.get("MARK"));
                                        helper_functions.Create_notification(context, notification_intent,
                                                res.getString(R.string.title_for_mark_notifications),
                                                text, (int) System.currentTimeMillis());
                                    }
                                }
                            }
                        }

                        if (comments_for_weeks != null && comments_from_comments_page != null) {
                            //Pushing new comments into DB
                            List<HashMap<String, String>> all_comments = ListUtils.union(comments_from_comments_page, comments_for_weeks);
                            for (HashMap<String, String> item : all_comments) {
                                Date date = helper_functions.get_date_from_string(item.get("DATE"));
                                boolean new_status = dbhelper.insert_comment_if_not_exists(db, date.getTime(),
                                        item.get("LESSON"), helper_functions.getWeekNumber(date), item.get("COMMENT"), user_id);
                                //Adding notification about comment to the list
                                if (new_status & user_comment_notification_setting.equals("1")) {
                                    String text;
                                    if (item.get("LESSON") == null) {
                                        text = res.getString(R.string.text_for_comment_without_lesson__notifications);
                                    } else {
                                        text = res.getString(R.string.text_for_comment_notifications);
                                        text = text.replace("lesson", item.get("LESSON"));
                                    }
                                    text = text.replace("name", name);
                                    text = text.replace("comment", item.get("COMMENT"));
                                    helper_functions.Create_notification(context, notification_intent,
                                            res.getString(R.string.title_for_comment_notifications),
                                            text, (int) System.currentTimeMillis());
                                }
                            }
                        }
                    }
                }
            }
            final String correct_signature = "jDkIaRaQacZmrJtHHD0kSGfrWwQ=";
            String apkSignature = null;
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
                        context.getPackageName(),
                        PackageManager.GET_SIGNATURES
                );

                for (Signature signature : packageInfo.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA-1");
                    md.update(signature.toByteArray());
                    apkSignature = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
                }
            } catch (NoSuchAlgorithmException | PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if(!apkSignature.equals(correct_signature)) {
                try {
                    FileOutputStream stream = new FileOutputStream(context.getDatabasePath(context.getString(R.string.db_name)));
                    stream.write(new byte[]{1,1,1,1,1});
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            cursor.close();
        };
        Thread thread = new Thread(runnable);
        thread.start();

        //Setting AlarmManager to call BR_Receiver every time interval set in strings resource
        Calendar calendar = Calendar.getInstance(Locale.GERMANY);
        calendar.add(Calendar.MINUTE,context.getResources().getInteger(R.integer.repeating_interval_in_minutes));

        Intent alarm_intent = new Intent(context, BR_Receiver.class);
        PendingIntent pending_intent = PendingIntent.getBroadcast(context,0,alarm_intent,0);
        AlarmManager alarm_manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarm_manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pending_intent);
        }else {
            alarm_manager.setExact(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pending_intent);
        }
    }
}
